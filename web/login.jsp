<%--suppress XmlUnboundNsPrefix --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="root" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<root:root-page>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <c:if test="${not empty param['err'] }">
                    <div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <fmt:message key="login.wrong-credentials"/>
                    </div>
                </c:if>
                <div class="well well-lg">
                    <form class="form-horizontal" action="j_security_check" method="post">
                        <fieldset>
                            <legend><fmt:message key="login.formname"/></legend>
                            <div class="form-group">
                                <label for="login-input" class="col-lg-2 control-label">
                                    <fmt:message key="login.login-field"/>
                                </label>
                                <div class="col-lg-10">
                                    <input id="login-input" name="j_username" type="text" class="form-control"
                                           placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password-input" class="col-lg-2 control-label">
                                    <fmt:message key="login.password-field"/>
                                </label>
                                <div class="col-lg-10">
                                    <input id="password-input" name="j_password" type="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <input type="submit" value="<fmt:message key='login.formname'/>"
                                       class="btn btn-success">
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <script>
        let errorBlockHeader = "<fmt:message key='application.error.descr'/>";

        let loginErrMsg = '<fmt:message key="login.login-field"/>: <fmt:message key="admin.register.doctor.error.login.descr"/>';
        let passwordErrMsg = '<fmt:message key="login.password-field"/>: <fmt:message key="admin.register.doctor.error.password.descr"/>';
    </script>
    <script src="/assets/js/login-validation.js"></script>
</root:root-page>
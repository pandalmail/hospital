let $login = $('input[name="j_username"]');
let $password = $('input[name="j_password"]');
let $errorPlace = $('form').parent().parent();

$('input[type="submit"]').click((e) => {
    let errorMessages = [];

    $login.parent().removeClass("has-error");
    $password.parent().removeClass("has-error");
    $('#error-block').remove();

    // not only ascii
    let loginHasErrors = !/^([\w\d]|[\u0400-\u04FF]){1,20}$/iu.test($login.val());
    let passwordHasErrors = !/^(?=.*?[\u0400-\u04FF\w])(?=.*?\d).{6,20}$/u.test($password.val());

    if (loginHasErrors) {
        $login.parent().addClass("has-error");
        errorMessages.push(loginErrMsg);
    }
    if (passwordHasErrors) {
        $password.parent().addClass("has-error");
        errorMessages.push(passwordErrMsg);
    }

    if (loginHasErrors || passwordHasErrors) {
        $errorPlace.prepend(generateErrorBock(errorBlockHeader, errorMessages));
        e.preventDefault();
    }
});

function generateErrorBock(errorBlockHeader, errorMessages) {
    'use strict';
    let errorsTemplate = '';
    for (let i = 0; i < errorMessages.length; i++) {
        errorsTemplate += '<li>'+errorMessages[i]+'</li>';
    }
    return `<div id="error-block" class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>${errorBlockHeader}</strong>
                        <ul>
                           ${errorsTemplate}
                        </ul>
                    </div>`;
}

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="root" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<root:root-page>
    <div class="container-fluid">
        <div class="row">
            <h1 class="lightweight text-center"><fmt:message key="index.welcome"/></h1>
            <div class="col-xs-4 col-xs-offset-5">
                <span style="font-size: 6em; color:  rgb(76, 175, 80);"
                      class="text-center glyphicon glyphicon-plus-sign"></span>
            </div>
        </div>
    </div>
</root:root-page>
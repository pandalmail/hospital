<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag pageEncoding="utf-8" %>

<%@ attribute name="doctorsByCategories" type="java.util.Map" required="true" %>
<%@ attribute name="modifying" type="java.lang.Boolean" %>
<%@ attribute name="headerText" type="java.lang.String" %>
<%@ attribute name="descriptionText" type="java.lang.String" %>
<%@ attribute name="withRadio" type="java.lang.Boolean" %>

<c:url var="docsByNameUrl" value="">
    <%--list previous params--%>
    <c:forEach items="${param}" var="urlParam">
        <c:if test="${urlParam.key ne 'dsort'}">
            <c:param name="${urlParam.key}" value="${urlParam.value}"/>
        </c:if>
    </c:forEach>
    <c:param name="dsort" value="name"/>
</c:url>
<c:url var="docsByPatientsCountUrl" value="">
    <%--list previous params--%>
    <c:forEach items="${param}" var="urlParam">
        <c:if test="${urlParam.key ne 'dsort'}">
            <c:param name="${urlParam.key}" value="${urlParam.value}"/>
        </c:if>
    </c:forEach>
    <c:param name="dsort" value="patients"/>
</c:url>
<c:url var="docsByCategoriesUrl" value="">
    <%--list previous params--%>
    <c:forEach items="${param}" var="urlParam">
        <c:if test="${urlParam.key ne 'dsort'}">
            <c:param name="${urlParam.key}" value="${urlParam.value}"/>
        </c:if>
    </c:forEach>
    <c:param name="dsort" value="category"/>
</c:url>
<c:url var="doctorsAscUrl" value="">
    <%--list previous params--%>
    <c:forEach items="${param}" var="urlParam">
        <c:if test="${urlParam.key ne 'd_order'}">
            <c:param name="${urlParam.key}" value="${urlParam.value}"/>
        </c:if>
    </c:forEach>
    <c:param name="d_order" value="asc"/>
</c:url>
<c:url var="doctorsDescUrl" value="">
    <%--list previous params--%>
    <c:forEach items="${param}" var="urlParam">
        <c:if test="${urlParam.key ne 'd_order'}">
            <c:param name="${urlParam.key}" value="${urlParam.value}"/>
        </c:if>
    </c:forEach>
    <c:param name="d_order" value="desc"/>
</c:url>
<fmt:message key="admin.section.doctors" var="defaultHeaderText"/>
<fmt:message key="admin.section.doctors.descr" var="defaultDescrText"/>

<div class="page-header">
    <div class="btn-group pull-right">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false">
            <fmt:message key="application.order"/>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="${doctorsAscUrl}"><fmt:message key="application.order.asc"/></a></li>
            <li><a href="${doctorsDescUrl}"><fmt:message key="application.order.desc"/></a></li>
        </ul>
    </div>
    <div class="btn-group pull-right">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                aria-haspopup="true"
                aria-haspopup="true"
                aria-expanded="false">
            <fmt:message key="application.sort"/> <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="${docsByNameUrl}"><fmt:message key="application.sort.name"/></a></li>
            <li><a href="${docsByCategoriesUrl}"><fmt:message key="application.sort.category"/></a></li>
            <li><a href="${docsByPatientsCountUrl}"><fmt:message key="application.sort.patient-count"/></a>
            </li>
        </ul>
    </div>
    <h3>
        <c:out value="${headerText}" default="${defaultHeaderText}"/>
        <small>
            <c:out value="${descriptionText}" default="${defaultDescrText}"/>
        </small>
    </h3>
</div>
<div>
    <div class="doc-spec-container">
        <c:if test="${modifying}">
            <form action="/admin/add/category" method="post"
                  style="vertical-align: top; margin: 5px; display: inline-block">
                <input type="hidden" name="validation" value="register.category">
                <input name="category" style="margin: 5px" class="form-control" type="text"
                       placeholder="<fmt:message key="admin.register.category.name-field"/>">
                <button type="submit" class="btn btn-sm btn-success add-btn btn-block">
                    <span class="glyphicon glyphicon-plus-sign"></span>
                    <fmt:message key="admin.section.doctors.add-spec-button"/>
                </button>
            </form>
        </c:if>
        <c:forEach var="categoryEntry" items="${doctorsByCategories}">
            <div class="doc-spec-card">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center"><h5>${categoryEntry.key}</h5></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test="${modifying}">
                        <tr>
                            <td>
                                <a href="/admin/add/doctor">
                                    <button class="btn btn-sm btn-success btn-block">
                                        <span class="glyphicon glyphicon-plus-sign"></span>
                                        <fmt:message key="admin.section.doctors.add-doc-button"/>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    </c:if>
                    <c:forEach items="${categoryEntry.value}" var="doc">
                        <tr>
                            <c:if test="${not withRadio}">
                                <td>${doc.name} (${doc.patientsCount})</td>
                            </c:if>

                            <c:if test="${withRadio}">
                                <td>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="docId" value="${doc.id}">
                                                ${doc.name} (${doc.patientsCount})
                                        </label>
                                    </div>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:forEach>
    </div>
</div>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ tag pageEncoding="utf-8" %>
<%@attribute name="patient" required="true" type="ua.nure.apukhtin.hospital.model.Patient" %>
<%@attribute name="expanded" required="false" %>

<div class="panel panel-success panel-default">
    <div class="panel-heading" role="tab" id="heading_${patient.id}">
        <h4 class="panel-title">
            <a class="lightweight text-uppercase" role="button" data-toggle="collapse"
               data-parent="#accordion"
               href="#collapse_${patient.id}" aria-expanded="${expanded}"
               aria-controls="collapse_${patient.id}">
                ${patient.name}
            </a>
        </h4>
    </div>
    <div id="collapse_${patient.id}" class="panel-collapse ${expanded ? '' : 'collapse'}"
         role="tabpanel"
         aria-labelledby="heading_${patient.id}">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3">
                    <b><fmt:message key="patient.card"/>:</b>
                </div>
                <div class="col-sm-3">
                    <a class="underlined" href="/patient/card/${patient.id}"><fmt:message key="patient.card.value"/></a>
                </div>
                <div class="col-sm-3">
                    <b><fmt:message key="patient.birth"/></b>
                </div>
                <div class="col-sm-3">
                    <fmt:formatDate value="${patient.birthDate}"/>
                </div>
                <div class="col-sm-12">
                    <hr style="margin: 5px 0">
                </div>
                <div class="col-sm-3"><b><fmt:message key="patient.assigned"/>:</b></div>
                <div class="col-sm-9">
                    <c:out value="${patient.doctor.name}"
                           default="N/A"/>

                </div>
                <div class="col-sm-12">
                    <c:url var="newDiagnosisUrl" value="/doctor/assign">
                        <c:param name="pid" value="${patient.id}"/>
                    </c:url>
                    <c:url var="assignURL" value="/set/doctor">
                        <c:param name="pid" value="${patient.id}"/>
                    </c:url>
                    <tags:in-role roles="doctor">
                        <a href="${newDiagnosisUrl}" class="pull-left underlined">
                            <fmt:message key="diagnosis.add"/></a>
                    </tags:in-role>
                    <tags:in-role roles="doctor,admin">
                        <a class="underlined pull-right" href="${assignURL}">
                            <fmt:message key="doctor.assign"/>
                        </a>
                    </tags:in-role>
                </div>
            </div>
        </div>
    </div>
</div>

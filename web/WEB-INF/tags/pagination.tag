<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag pageEncoding="utf-8" %>

<%@ attribute name="paginationParam" required="true" type="java.lang.String" %>
<%@ attribute name="lastPage" required="true" %>

<c:choose>
    <c:when test="${empty param[paginationParam]}">
        <c:set value="1" var="currentPage"/>
    </c:when>
    <c:when test="${not empty param[paginationParam]}">
        <c:set value="${param[paginationParam]}" var="currentPage"/>
    </c:when>
</c:choose>

<c:set var="lastPage" value="${empty lastPage ? 1 : lastPage}"/>

<c:url var="first" value="">
    <%--list previous params--%>
    <c:forEach items="${param}" var="urlParam">
        <c:if test="${urlParam.key ne paginationParam}">
            <c:param name="${urlParam.key}" value="${urlParam.value}"/>
        </c:if>
    </c:forEach>
    <c:param name="${paginationParam}" value="1"/>
</c:url>
<c:url var="last" value="">
    <%--list previous params--%>
    <c:forEach items="${param}" var="urlParam">
        <c:if test="${urlParam.key ne paginationParam}">
            <c:param name="${urlParam.key}" value="${urlParam.value}"/>
        </c:if>
    </c:forEach>
    <c:param name="${paginationParam}" value="${lastPage}"/>
</c:url>

<ul class="pagination pagination-sm">
    <li><a href="${first}">&laquo;</a></li>
    <c:forEach begin="${currentPage - 1}" end="${currentPage + 1}" step="1" var="i">
        <c:if test="${i ge 1 and i le lastPage}">
            <c:url var="currentPage" value="">
                <%--list previous params--%>
                <c:forEach items="${param}" var="urlParam">
                    <c:if test="${urlParam.key ne paginationParam}">
                        <c:param name="${urlParam.key}" value="${urlParam.value}"/>
                    </c:if>
                </c:forEach>
                <c:param name="${paginationParam}" value="${i}"/>
            </c:url>
            <li>
                <a href="${currentPage}">${i}</a>
            </li>
        </c:if>
    </c:forEach>
    <li><a href="${last}">&raquo;</a></li>
</ul>
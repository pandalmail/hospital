<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag pageEncoding="utf-8" %>
<%@ attribute name="roles" required="true" %>

<c:set var="hasAccess" value="false"/>
<c:forTokens delims="," items="${roles}" var="role">
    <c:set var="hasAccess" value="${hasAccess || pageContext.request.isUserInRole(role)}"/>
</c:forTokens>
<c:if test="${hasAccess}">
    <jsp:doBody/>
</c:if>
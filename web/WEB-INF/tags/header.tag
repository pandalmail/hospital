<%@ tag pageEncoding="utf-8"    %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dyn-header" uri="http://apukhtin.hosptial.nure.ua" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="isAuthorized" value="${not empty pageContext.request.userPrincipal}"/>
<c:set var="principle" value="${pageContext.request.userPrincipal.name}"/>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                <fmt:message key="application.name"/>
            </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">${sessionScope['javax.servlet.jsp.jstl.fmt.locale.session']}<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li class="text-center"><fmt:message key="header.specify-lang"/></li>
                    <li class="divider"></li>
                    <li><a href="/set-locale.jsp?locale=ru">RU</a></li>
                    <li><a href="/set-locale.jsp?locale=en">EN</a></li>
                </ul>
            </li>
            <c:if test="${isAuthorized}">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">${user.name}<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="text-center">${principle}</li>
                        <li class="divider"></li>
                        <li><a href="/home"><fmt:message key="header.home"/></a></li>
                        <dyn-header:dynamic-menu var="mappings"/>
                        <c:forEach var="mapping" items="${mappings}">
                            <li><a href="${mapping.value}"><fmt:message key="${mapping.key}"/></a></li>
                        </c:forEach>
                        <li role="separator" class="divider"></li>
                        <li><a href="/logout"><fmt:message key="header.logout"/></a></li>
                    </ul>
                </li>
            </c:if>
            <c:if test="${!isAuthorized}">
                <li><a href="/home"><fmt:message key="header.login"/></a></li>
                <li><a href="/register"><fmt:message key="header.register.patient"/></a></li>
            </c:if>
        </ul>
    </div>
</nav>

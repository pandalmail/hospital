<%@ tag pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<%@ attribute name="patients" type="java.lang.Iterable" required="true" %>
<%@ attribute name="maxPatientsPage" type="java.lang.Integer" required="true" %>
<%@ attribute name="headerText" type="java.lang.String" %>
<%@ attribute name="descriptionText" type="java.lang.String" %>

<fmt:message key="admin.section.patients" var="defaultHeaderText" scope="page"/>
<fmt:message key="admin.section.patients.descr" var="defaultDescriptionText" scope="page"/>

<c:url var="patientsByName" value="">
    <%--list previous params--%>
    <c:forEach items="${param}" var="urlParam">
        <c:if test="${urlParam.key ne 'psort'}">
            <c:param name="${urlParam.key}" value="${urlParam.value}"/>
        </c:if>
    </c:forEach>
    <c:param name="psort" value="name"/>
</c:url>
<c:url var="patientsByDate" value="">
    <%--list previous params--%>
    <c:forEach items="${param}" var="urlParam">
        <c:if test="${urlParam.key ne 'psort'}">
            <c:param name="${urlParam.key}" value="${urlParam.value}"/>
        </c:if>
    </c:forEach>
    <c:param name="psort" value="date"/>
</c:url>
<c:url var="patientsAscUrl" value="">
    <%--list previous params--%>
    <c:forEach items="${param}" var="urlParam">
        <c:if test="${urlParam.key ne 'p_order'}">
            <c:param name="${urlParam.key}" value="${urlParam.value}"/>
        </c:if>
    </c:forEach>
    <c:param name="p_order" value="asc"/>
</c:url>
<c:url var="patientsDescUrl" value="">
    <%--list previous params--%>
    <c:forEach items="${param}" var="urlParam">
        <c:if test="${urlParam.key ne 'p_order'}">
            <c:param name="${urlParam.key}" value="${urlParam.value}"/>
        </c:if>
    </c:forEach>
    <c:param name="p_order" value="desc"/>
</c:url>

<div class="page-header">
    <div class="btn-group pull-right">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false">
            <fmt:message key="application.order"/>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="${patientsAscUrl}"><fmt:message key="application.order.asc"/></a></li>
            <li><a href="${patientsDescUrl}"><fmt:message key="application.order.desc"/></a></li>
        </ul>
    </div>
    <div class="btn-group pull-right">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false">
            <fmt:message key="application.sort"/>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="${patientsByName}"><fmt:message key="application.sort.name"/></a></li>
            <li><a href="${patientsByDate}"><fmt:message key="application.sort.date"/></a></li>
        </ul>
    </div>
    <h3>
        <c:out value="${headerText}" default="${defaultHeaderText}"/>
        <small>
            <c:out value="${descriptionText}" default="${defaultDescriptionText}"/>
        </small>
    </h3>
</div>
<div class="col-sm-8 col-sm-offset-2">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <c:forEach var="patient" items="${patients}">
            <tags:patient patient="${patient}"/>
        </c:forEach></div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <tags:pagination paginationParam="patientPage" lastPage="${maxPatientsPage}"/>
        </div>
    </div>
</div>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag pageEncoding="utf-8" %>
<%@ attribute name="nurses" type="java.util.Collection" required="true" %>

<div class="page-header">
    <h3>
        <fmt:message key="nurses"/>
        <small>
            <fmt:message key="admin.section.nurses.descr"/>
        </small>
    </h3>
</div>
<div class="doc-spec-container">
    <div class="doc-spec-card">
        <table class="table">
            <thead>
            <tr>
                <th class="text-center"><h5><fmt:message key="nurses"/></h5></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <a href="/admin/add/nurse">
                        <button class="btn btn-sm btn-success btn-block">
                            <span class="glyphicon glyphicon-plus-sign"></span>
                            <fmt:message key="admin.section.nurses.add-nurse-button"/>
                        </button>
                    </a>
                </td>
            </tr>
            <c:forEach items="${nurses}" var="nurse">
                <tr>
                    <td>${nurse.name}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

</div>
<%@ tag pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dyn-header" uri="http://apukhtin.hosptial.nure.ua" %>
<%@ taglib prefix="head" tagdir="/WEB-INF/tags" %>

<c:if test="${empty sessionScope['javax.servlet.jsp.jstl.fmt.locale.session']}">
    <c:set var='javax.servlet.jsp.jstl.fmt.locale.session' value="en" scope="session"/>
</c:if>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="application.name"/></title>

    <link href="/assets/dist/bootstrap-paper/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/bootstrap-customize.css">
    <link rel="stylesheet" href="/assets/css/core.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="/assets/dist/jquery/jquery-3.1.1.js"></script>
    <script src="/assets/dist/bootstrap-paper/js/bootstrap.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>

</head>
<body>
<head:header/>
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <c:if test="${not empty errors}">
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><fmt:message key="application.error.descr"/></strong>
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <li><fmt:message key="${error.key}"/>: <fmt:message key="${error.value}"/></li>
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
            <c:if test="${not empty successMsg}">
                <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <fmt:message key="${successMsg}"/>
                </div>
            </c:if>
        </div>
    </div>
</div>
<jsp:doBody/>

<c:remove var="errors"/>
<c:remove var="successMsg"/>

</body>
</html>
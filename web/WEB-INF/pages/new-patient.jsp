<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="root" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<root:root-page>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <div class="well well-lg">
                    <form class="form-horizontal" method="post">
                        <fieldset>
                            <legend><fmt:message key="admin.register.patient.formname"/></legend>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><fmt:message key="login.login-field"/></label>
                                <div class="col-lg-10">
                                    <input type="text" name="login" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><fmt:message key="login.password-field"/></label>
                                <div class="col-lg-10">
                                    <input type="password" name="password" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><fmt:message key="patient.name"/></label>
                                <div class="col-lg-10">
                                    <input type="text" name="name" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><fmt:message key="patient.birth"/></label>
                                <div class="col-lg-10">
                                    <input type="date" name="birth" ащ class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <div class="g-recaptcha" data-sitekey="6Lf3GxMUAAAAADh9xaS8SVr9hLBXZPjAnB8zmbD6"></div>
                                <button type="submit" class="btn btn-success">
                                    <fmt:message key="admin.register.button"/>
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</root:root-page>
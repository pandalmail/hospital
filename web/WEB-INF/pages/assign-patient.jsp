<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<fmt:message key="admin.section.doctors" var="headerText"/>
<fmt:message key="doctor.assign.descr" var="descriptionText"/>

<tags:root-page>
    <div class="container">
        <div class="row">
            <h2 class="text-center"><fmt:message key="doctor.assign"/></h2>
            <div class="col-sm-8 col-sm-offset-2">
                <c:if test="${not empty patient}">
                   <tags:patient patient="${patient}" expanded="true"/>
                </c:if>
            </div>
            <div class="clearfix"></div>
            <form method="post">
                <tags:doctors doctorsByCategories="${doctorsByCategories}"
                              headerText="${headerText}" descriptionText="${descriptionText}"
                              withRadio="true"/>
                <input type="submit" value="<fmt:message key="doctor.assign"/>"
                       class="btn btn-sm btn-success center-block">
            </form>
        </div>
    </div>
</tags:root-page>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:message key="doctor.assigned" var="assignedPatientsMsg"/>
<fmt:message key="doctor.assigned.descr" var="assignedPatientsDescrMsg"/>

<tags:root-page>
    <div class="container">
        <div class="row">
            <tags:patients patients="${patients}" maxPatientsPage="${maxPatientsPage}" headerText="${assignedPatientsMsg}"/>
        </div>
    </div>
</tags:root-page>

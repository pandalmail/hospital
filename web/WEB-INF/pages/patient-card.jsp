<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tags:root-page>
  <div class="modal fade">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">
            <fmt:message key="diagnosis.final.select"/>
          </h4>
        </div>
        <form action="/patient/discharge" method="post">
          <input type="hidden" name="pid" value="${patient.id}">
          <div class="modal-body">
            <c:forEach items="${illnessPeriodDTOs[0].diagnoses}" var="diagnosis">
              <div class="radio">
                <label>
                  <input type="radio" name="diagnosisId" value="${diagnosis.key.id}">
                    ${diagnosis.key.name}
                </label>
              </div>
            </c:forEach>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-sm btn-success"><fmt:message
                key="patient.discharge"/></button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="page-header">
        <h3>
          <fmt:message key="patient.card"/>
          <small>${patient.name}</small>
        </h3>
      </div>

      <div class="col-sm-8 col-sm-offset-2">
        <tags:patient patient="${patient}" expanded="true"/>
      </div>
      <tags:in-role roles="admin,doctor">
        <div class="col-sm-3">
          <button class="btn btn-sm btn-success"
              data-toggle="modal" data-target=".modal" ${patient.discharged? 'disabled': ''}>
            <fmt:message key="patient.discharge"/>
          </button>
        </div>
      </tags:in-role>
      <c:if test="${not empty illnessPeriodDTOs}">
        <div class="col-sm-5 pull-right text-right">
          <tags:pagination paginationParam="periodsPage" lastPage="${periodsLastPage}"/>
        </div>
        <p></p> <!--some margin-->
        <table class="table table-striped">
          <tbody>
          <c:forEach items="${illnessPeriodDTOs}" var="illnessPeriodDTO">
            <tr>
              <td>
                <h5 class="text-center"><fmt:message key="illnessPeriod"/>
                  <fmt:formatDate value="${illnessPeriodDTO.illnessPeriod.startDate}" var="start"/>
                  <fmt:formatDate value="${illnessPeriodDTO.illnessPeriod.endDate}" var="end"/>

                  <fmt:message key="from"/> <c:out value="${start}"/>
                  <fmt:message key="to"/> <c:out value="${end}" default="..."/>
                </h5>
                <hr>
                <div class="row">
                  <div class="col-sm-2 underlined"><fmt:message key="patient.diagnosis.final"/></div>
                  <!--Max: 60 symbols-->
                  <div class="col-sm-7">
                    <c:out value="${illnessPeriodDTO.illnessPeriod.finalDiagnosis}"
                         default="N/A"/></div>
                  <div class="col-sm-3">
                    <!-- must have <= 21 symbols!-->
                    <c:url value="/patient/report" var="cardUrl">
                      <c:param name="pid" value="${patient.id}"/>
                      <c:param name="period_id" value="${illnessPeriodDTO.illnessPeriod.id}"/>
                    </c:url>
                    <a href="${cardUrl}" class="underlined"><fmt:message key="patient.inquiry"/></a>
                    (PDF)
                  </div>
                  <div class="col-sm-12">
                    <hr>
                    <div class="col-sm-4"><span style="font-size: 20px"><fmt:message
                        key="diagnoses"/></span></div>
                    <div class="col-sm-8 text-center"><span style="font-size: 20px"><fmt:message
                        key="prescriptions"/></span>
                    </div>
                    <div class="row">
                      <c:forEach var="diagnosis" items="${illnessPeriodDTO.diagnoses}">
                        <div class="diagnoses">
                          <div class="col-sm-12">
                            <hr>
                          </div>
                          <div class="col-sm-4"><span
                              class="underlined"><b>${diagnosis.key.name}</b></span></div>
                          <div class="col-sm-8">
                            <div class="prescriptions">
                              <div class="panel-group" id="accordion-${diagnosis.key.id}"
                                 role="tablist"
                                 aria-multiselectable="true">
                                <c:if test="${not empty diagnosis.value.drugs}">
                                  <div class="panel panel-success panel-default">
                                    <div class="panel-heading" role="tab"
                                       id="heading-drug-${diagnosis.key.id}">
                                      <h4 class="panel-title">
                                        <a class="collapsed" role="button"
                                           data-toggle="collapse"
                                           data-parent="#accordion-${diagnosis.key.id}"
                                           href="#collapse-drug-${diagnosis.key.id}"
                                           aria-expanded="false"
                                           aria-controls="collapse-drug-${diagnosis.key.id}">
                                          <fmt:message key="drugs"/>
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapse-drug-${diagnosis.key.id}"
                                       class="panel-collapse collapse"
                                       role="tabpanel"
                                       aria-labelledby="heading-drug-${diagnosis.key.id}">
                                      <div class="panel-body">
                                        <ul class="list-group">
                                          <c:forEach
                                              items="${diagnosis.value.drugs}"
                                              var="drug">
                                            <li class="list-group-item">
                                                ${drug.name}
                                              <c:if test="${not drug.done}">
                                                <c:url var="executeDrug"
                                                     value="/execute">
                                                  <c:param
                                                      name="prescription"
                                                      value="drugs"/>
                                                  <c:param
                                                      name="prescriptionId"
                                                      value="${drug.id}"/>
                                                </c:url>
                                                <tags:in-role
                                                    roles="doctor,nurse">
                                                  <form action="${executeDrug}"
                                                      method="post">
                                                    <button
                                                        style="top: -25px;"
                                                        type="submit"
                                                        class="btn btn-sm btn-success pull-right">
                                                      <fmt:message
                                                          key="prescription.execute"/>
                                                    </button>
                                                  </form>
                                                </tags:in-role>
                                              </c:if>
                                              <c:if test="${drug.done}">
                                                <span style="color: forestgreen"
                                                    class="glyphicon glyphicon-ok pull-right"></span>
                                              </c:if>
                                            </li>
                                          </c:forEach>
                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                                </c:if>
                                <c:if test="${not empty diagnosis.value.procedures}">
                                  <div class="panel panel-success panel-default">
                                    <div class="panel-heading" role="tab"
                                       id="heading-proc-${diagnosis.key.id}">
                                      <h4 class="panel-title">
                                        <a class="collapsed" role="button"
                                           data-toggle="collapse"
                                           data-parent="#accordion-${diagnosis.key.id}"
                                           href="#collapse-proc-${diagnosis.key.id}"
                                           aria-expanded="false"
                                           aria-controls="collapse-proc-${diagnosis.key.id}">
                                          <fmt:message key="procedures"/>
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapse-proc-${diagnosis.key.id}"
                                       class="panel-collapse collapse"
                                       role="tabpanel"
                                       aria-labelledby="heading-proc-${diagnosis.key.id}">
                                      <div class="panel-body">
                                        <ul class="list-group">
                                          <c:forEach
                                              items="${diagnosis.value.procedures}"
                                              var="procedure">
                                            <li class="list-group-item">
                                                ${procedure.name}
                                              <c:if test="${not procedure.done}">
                                                <c:url var="executeDrug"
                                                     value="/execute">
                                                  <c:param
                                                      name="prescription"
                                                      value="procedures"/>
                                                  <c:param
                                                      name="prescriptionId"
                                                      value="${procedure.id}"/>
                                                </c:url>
                                                <tags:in-role
                                                    roles="doctor,nurse">
                                                  <form action="${executeDrug}"
                                                      method="post">
                                                    <button
                                                        style="top: -25px;"
                                                        type="submit"
                                                        class="btn btn-sm btn-success pull-right">
                                                      <fmt:message
                                                          key="prescription.execute"/>
                                                    </button>
                                                  </form>
                                                </tags:in-role>
                                              </c:if>
                                              <c:if test="${procedure.done}">
                                                <span style="color: forestgreen"
                                                    class="glyphicon glyphicon-ok pull-right"></span>
                                              </c:if>
                                            </li>
                                          </c:forEach>
                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                                </c:if>
                                <c:if test="${not empty diagnosis.value.operations}">
                                  <div class="panel panel-success panel-default">
                                    <div class="panel-heading" role="tab"
                                       id="heading-opertn-${diagnosis.key.id}">
                                      <h4 class="panel-title">
                                        <a class="collapsed" role="button"
                                           data-toggle="collapse"
                                           data-parent="#accordion-${diagnosis.key.id}"
                                           href="#collapse-opertn-${diagnosis.key.id}"
                                           aria-expanded="false"
                                           aria-controls="collapse-opertn-${diagnosis.key.id}">
                                          <fmt:message key="operations"/>
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapse-opertn-${diagnosis.key.id}"
                                       class="panel-collapse collapse"
                                       role="tabpanel"
                                       aria-labelledby="heading-opertn-${diagnosis.key.id}">
                                      <div class="panel-body">
                                        <ul class="list-group">
                                          <c:forEach
                                              items="${diagnosis.value.operations}"
                                              var="operation">
                                            <li class="list-group-item">
                                                ${operation.name}
                                              <c:if test="${not operation.done}">
                                                <c:url var="executeDrug"
                                                     value="/execute">
                                                  <c:param
                                                      name="prescription"
                                                      value="operations"/>
                                                  <c:param
                                                      name="prescriptionId"
                                                      value="${operation.id}"/>
                                                </c:url>
                                                <tags:in-role
                                                    roles="doctor">
                                                  <form action="${executeDrug}"
                                                      method="post">
                                                    <button
                                                        style="top: -25px;"
                                                        type="submit"
                                                        class="btn btn-sm btn-success pull-right">
                                                      <fmt:message
                                                          key="prescription.execute"/>
                                                    </button>
                                                  </form>
                                                </tags:in-role>
                                              </c:if>
                                              <c:if test="${operation.done}">
                        <span style="color: forestgreen"
                            class="glyphicon glyphicon-ok pull-right"></span>
                                              </c:if>
                                            </li>
                                          </c:forEach>
                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                                </c:if>
                              </div>
                            </div>
                          </div>
                        </div>
                      </c:forEach>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          </c:forEach></tbody>
        </table>
      </c:if>
      <c:if test="${empty illnessPeriodDTOs}">
        <h4 class="lightweight text-center">
          <fmt:message key="illnessPeriods.empty"/>
        </h4>
      </c:if>
    </div>
  </div>
</tags:root-page>
<%@page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tags:root-page>
    <div class="container">
        <div class="row">
            <h2 class="text-center"><fmt:message key="admin.dashboard"/></h2>
            <hr>
            <tags:doctors doctorsByCategories="${doctorsByCategories}" modifying="true"/>
            <tags:nurses nurses="${nurses}"/>
            <tags:patients patients="${patients}" maxPatientsPage="${maxPatientsPage}"/>
            <div class="col-sm-2">
                <a href="/admin/add/patient">
                    <button class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus-sign"></span>
                        <fmt:message key="admin.section.patients.add-btn"/>
                    </button>
                </a>
            </div>
        </div>
</tags:root-page>
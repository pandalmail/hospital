<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="root" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<root:root-page>
    <c:set var="login-err" value="${not empty errors['admin.register.doctor.error.login']? 'has-error' : ''}"
           scope="page"/>
    <c:set var="password-err" value="${not empty errors['admin.register.doctor.error.password']? 'has-error' : ''}"/>
    <c:set var="name-err" value="${not empty errors['admin.register.doctor.error.name']? 'has-error' : ''}"/>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <div class="well well-lg">
                    <form class="form-horizontal" method="post">
                        <input type="hidden" name="validation" value="register.doctor">
                        <fieldset>
                            <c:if test="${not forNurse or empty forNurse}">
                                <legend><fmt:message key="admin.register.doctor.formname"/></legend>
                            </c:if>
                            <c:if test="${forNurse}">
                                <legend><fmt:message key="admin.register.nurse.formname"/></legend>
                            </c:if>
                            <div class="
                                form-group ${not empty errors['admin.register.doctor.error.login']? 'has-error' : ''}">
                                <label class="col-lg-2 control-label"><fmt:message key="login.login-field"/></label>
                                <div class="col-lg-10">
                                    <input name="login" value="${sessionScope.login}" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group
                                ${not empty errors['admin.register.doctor.error.password']? 'has-error' : ''}">
                                <label class="col-lg-2 control-label"><fmt:message key="login.password-field"/></label>
                                <div class="col-lg-10">
                                    <input name="password" value="${sessionScope.password}" type="password"
                                           class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group
                                ${not empty errors['admin.register.doctor.error.name']? 'has-error' : ''}">
                                <label class="col-lg-2 control-label">
                                    <fmt:message key="admin.register.name-field"/>
                                </label>
                                <div class="col-lg-10">
                                    <input name="name" value="${sessionScope.name}" type="text" class="form-control">
                                </div>
                            </div>
                            <c:if test="${not forNurse or empty forNurse}">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="spec-select">
                                        <fmt:message key="admin.register.doctor.spec-field"/>
                                    </label>
                                    <div class="col-lg-10 ">
                                        <select name="doc-spec" class="form-control" id="spec-select">
                                            <c:forEach var="category" items="${categories}">
                                                <option value="${category}">${category}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </c:if>
                            <div class="form-group text-center">
                                <input type="submit" value="Register" class="btn btn-success">
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</root:root-page>
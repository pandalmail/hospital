<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<tags:root-page>
    <form method="post">
        <input style="display: inline-block; width: 90%;" type="hidden" name="pid" value="${patient.id}">
        <div class="container">
            <div class="row">
                <form class="form-horizontal">
                    <div class="page-header">
                        <h3>
                            <fmt:message key="diagnosis.new"/>
                            <small>${patient.name}</small>
                        </h3>
                    </div>
                    <div class="col-sm-2">
                        <h4 class="lightweight " style="display: inline-block;"> <fmt:message key="diagnosis"/>:</h4></div>
                    <div class="col-sm-10">
                        <input style="display: inline-block; width: 90%;" type="text" name="diagnosis"
                               style="width: 100%"
                               class="form-control" placeholder=" <fmt:message key="diagnosis.descr"/>">
                    </div>
                    <div class="col-sm-12">
                        <hr>
                    </div>
                    <div id="procedures" class="col-sm-4">
                        <h4 class="lightweight">
                            <fmt:message key="procedures"/> <span id="addProcedures" style="color:rgb(76, 175, 80)"
                                             class="add-btn glyphicon glyphicon-plus-sign"></span>
                        </h4>
                        <div>
                            <small> <fmt:message key="prescription.add.descr"/></small>
                        </div>
                    </div>
                    <div id="drugs" class="col-sm-4">
                        <h4 class="lightweight">
                            <fmt:message key="drugs"/> <span id="addGrugs" style="color:rgb(76, 175, 80)"
                                        class="add-btn glyphicon glyphicon-plus-sign"></span>
                        </h4>
                        <div>
                            <small> <fmt:message key="prescription.add.descr"/></small>
                        </div>
                    </div>
                    <div id="operations" class="col-sm-4">
                        <h4 class="lightweight">
                            <fmt:message key="operations"/> <span id="addOperations" style="color:rgb(76, 175, 80)"
                                             class="add-btn glyphicon glyphicon-plus-sign"></span>
                        </h4>
                        <div>
                            <small> <fmt:message key="prescription.add.descr"/></small>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <p></p>
                        <hr>
                        <button class="btn btn-sm btn-success"><fmt:message key="diagnosis.add"/></button>
                        <p></p>
                    </div>
                </form>
            </div>
        </div>
    </form>
    <script>
        function appendInputWithName(name) {
            $('#' + name).append(
                '<div><input style="display: inline-block; width: 90%;" name="' + name + '" type="text" class="form-control"><span class="remove-btn glyphicon glyphicon-remove-sign"></span></div>'
            );
            $('.remove-btn').click(deleteRowCallBack);
        }

        function deleteRowCallBack() {
            $(this).parent().remove();
        }

        $('#addOperations').click(() => appendInputWithName('operations'));
        $('#addGrugs').click(() => appendInputWithName('drugs'));
        $('#addProcedures').click(() => appendInputWithName('procedures'));

        $('.remove-btn').click(deleteRowCallBack);
    </script>
</tags:root-page>

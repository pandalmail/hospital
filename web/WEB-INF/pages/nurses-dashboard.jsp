<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<tags:root-page>
    <div class="container">
        <div class="row">
            <tags:patients patients="${patients}" maxPatientsPage="${maxPatientsPage}"/>
        </div>
    </div>
</tags:root-page>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<fmt:setLocale value="${param['locale']}" scope="session"/>
<c:set var="destination" value="${empty header['Referer'] ? '/' : header['Referer']}"/>
<c:set var="locale" value="${param['locale']}" scope="session"/>
<c:redirect url="${destination}"/>
package ua.nure.apukhtin.hospital.servlet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ua.nure.apukhtin.hospital.service.DoctorCategoryService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.DOCTOR_CATEGORY_SERVICE_ATTR;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.ERRORS_ATTR;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.SUCCESS_MSG_ATTR;

/**
 * Created by Vlad on 23.01.2017.
 */
public class AddDocSpecServletTest extends AbstractServletTest {

    private AddDocSpecServlet servlet = new AddDocSpecServlet();

    @Mock
    private DoctorCategoryService doctorCategoryService;

    @Before
    public void setUp() throws Exception {
        setBaseServlet(servlet);
        when(servletContext.getAttribute(DOCTOR_CATEGORY_SERVICE_ATTR)).thenReturn(doctorCategoryService);

        super.setUp();
    }

    @Test
    public void okBehaviour() throws Exception {
        enableWorkingService();
        servlet.doPost(request, response);

        verify(request.getSession(), never()).setAttribute(eq(ERRORS_ATTR), any());
        verify(request.getSession()).setAttribute(eq(SUCCESS_MSG_ATTR), any());
    }

    @Test
    public void testWithFailingService() throws Exception {
        enableFailingService();
        servlet.doPost(request, response);

        verify(request.getSession()).setAttribute(eq(ERRORS_ATTR), any());
        verify(request.getSession(), never()).setAttribute(eq(SUCCESS_MSG_ATTR), any());
    }

    private void enableWorkingService() throws ServiceException {
        doNothing().when(doctorCategoryService).create(anyString());
    }

    // todo wtf ?
    private void enableFailingService() throws ServiceException {
        doThrow(new ServiceException()).when(doctorCategoryService).create(anyString());
    }

}
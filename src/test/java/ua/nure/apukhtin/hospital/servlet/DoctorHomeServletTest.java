package ua.nure.apukhtin.hospital.servlet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import ua.nure.apukhtin.hospital.model.User;
import ua.nure.apukhtin.hospital.service.DoctorService;
import ua.nure.apukhtin.hospital.service.PatientService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.isNull;
import static org.mockito.Mockito.*;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;

/**
 * Created by Vlad on 23.01.2017.
 */
public class DoctorHomeServletTest extends AbstractServletTest {

    private static final long SOME_MAX_PAGE_VALUE = 3;
    private DoctorHomeServlet doctorHomeServlet;

    private User sessionUser = new User() {{
        setId(100500);
    }};

    @Mock
    private DoctorService doctorService;
    @Mock
    private PatientService patientService;


    @Before
    public void setUp() throws Exception {
        doctorHomeServlet = new DoctorHomeServlet();

        setBaseServlet(doctorHomeServlet);
        when(servletContext.getAttribute(DOCTOR_SERVICE_ATTR)).thenReturn(doctorService);
        when(servletContext.getAttribute(PATIENT_SERVICE_ATTR)).thenReturn(patientService);
        super.setUp();
    }

    @Test
    public void okBehaviour() throws Exception {
        enableWorkingServices();
        when(request.getSession().getAttribute(USER_ATTR)).thenReturn(sessionUser);

        doctorHomeServlet.doGet(request, response);

        verify(request).setAttribute(eq(PATIENTS_ATTR), anyList());
        verify(request).setAttribute(eq(MAX_PATIENT_PAGE_ATTR), anyLong());
        verify(request, never()).setAttribute(eq(ERRORS_ATTR), anyMap());
    }

    @Test
    public void testWithExceptionsInServices() throws Exception {
        enableNotWorkingServices();
        when(request.getSession().getAttribute(USER_ATTR)).thenReturn(sessionUser);

        doctorHomeServlet.doGet(request, response);

        ArgumentCaptor<Map> mapArgumentCaptor = ArgumentCaptor.forClass(Map.class);
        verify(request, never()).setAttribute(eq(PATIENTS_ATTR), any());
        verify(request).setAttribute(eq(MAX_PATIENT_PAGE_ATTR), isNull());

        verify(request).setAttribute(eq(ERRORS_ATTR), mapArgumentCaptor.capture());
        assertEquals(2, mapArgumentCaptor.getValue().size());
    }

    @Test
    public void failureTestWithoutSessionUser() throws Exception {
        enableWorkingServices();

        doctorHomeServlet.doGet(request, response);

        ArgumentCaptor<Map> mapArgumentCaptor = ArgumentCaptor.forClass(Map.class);
        verify(request, never()).setAttribute(eq(PATIENTS_ATTR), any());
        verify(request).setAttribute(eq(MAX_PATIENT_PAGE_ATTR), anyLong());

        verify(request).setAttribute(eq(ERRORS_ATTR), mapArgumentCaptor.capture());
        assertEquals(1, mapArgumentCaptor.getValue().size());
    }

    private void enableWorkingServices() throws ServiceException {
        when(doctorService.getAssignedPatients(anyLong())).thenReturn(Collections.emptyList());
        when(patientService.getMaxPageCount()).thenReturn(SOME_MAX_PAGE_VALUE);
    }

    private void enableNotWorkingServices() throws ServiceException {
        when(doctorService.getAssignedPatients(anyLong())).thenThrow(new ServiceException());
        when(patientService.getMaxPageCount()).thenThrow(new ServiceException());
    }

}
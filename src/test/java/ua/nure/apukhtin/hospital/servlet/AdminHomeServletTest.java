package ua.nure.apukhtin.hospital.servlet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ua.nure.apukhtin.hospital.service.DoctorService;
import ua.nure.apukhtin.hospital.service.PatientService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;

/**
 * Created by Vlad on 23.01.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class AdminHomeServletTest extends AbstractServletTest {

    private static final int EXPECTED_ERROR_SIZE = 3;

    private AdminHomeServlet adminHomeServlet;

    @Mock
    private DoctorService doctorService;
    @Mock
    private PatientService patientService;


    @Before
    public void setUp() throws Exception {
        adminHomeServlet = new AdminHomeServlet();

        setBaseServlet(adminHomeServlet);
        when(servletContext.getAttribute(DOCTOR_SERVICE_ATTR)).thenReturn(doctorService);
        when(servletContext.getAttribute(PATIENT_SERVICE_ATTR)).thenReturn(patientService);
        super.setUp();
    }

    @Test
    public void okBehaviour() throws Exception {
        enableWorkingServices();

        adminHomeServlet.doGet(request, response);

        verify(request).setAttribute(eq(DOCTORS_BY_CATEGORIES_ATTR), notNull());
        verify(request).setAttribute(eq(PATIENTS_ATTR), notNull());
        verify(request).setAttribute(eq(MAX_PATIENT_PAGE_ATTR), longThat(this::greaterThanZero));

        verify(request, never()).setAttribute(eq(ERRORS_ATTR), any());
    }

    @Test
    public void withExceptionsInAllServices() throws Exception {
        disableWorkingServices();
        adminHomeServlet.doGet(request, response);

        verify(request).setAttribute(eq(DOCTORS_BY_CATEGORIES_ATTR), isNull());
        verify(request).setAttribute(eq(PATIENTS_ATTR), isNull());
        verify(request).setAttribute(eq(MAX_PATIENT_PAGE_ATTR), isNull());

        ArgumentCaptor<Map> mapArgumentCaptor = ArgumentCaptor.forClass(Map.class);
        verify(request).setAttribute(eq(ERRORS_ATTR), mapArgumentCaptor.capture());
        assertEquals(EXPECTED_ERROR_SIZE, mapArgumentCaptor.getValue().size());
    }

    private void enableWorkingServices() throws ServiceException {
        when(doctorService.getDoctorsByCategories(any(Comparator.class))).thenReturn(Collections.emptyMap());
        when(patientService.getPatients(anyInt(), any(Comparator.class))).thenReturn(Collections.emptyList());
        when(patientService.getMaxPageCount()).thenReturn(5L);
    }

    private void disableWorkingServices() throws ServiceException {
        when(doctorService.getDoctorsByCategories(any(Comparator.class))).thenThrow(new ServiceException());
        when(patientService.getPatients(anyInt(), any(Comparator.class))).thenThrow(new ServiceException());
        when(patientService.getMaxPageCount()).thenThrow(new ServiceException());
    }


    private boolean greaterThanZero(Long integer) {
        return integer > 0;
    }

}

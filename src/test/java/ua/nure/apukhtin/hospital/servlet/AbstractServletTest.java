package ua.nure.apukhtin.hospital.servlet;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Vlad on 23.01.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class AbstractServletTest {

    @Mock
    protected RequestDispatcher requestDispatcher;
    @Mock
    protected ServletContext servletContext;
    @Mock
    protected HttpServletRequest request;
    @Mock
    protected HttpServletResponse response;
    @Mock
    protected HttpSession httpSession;

    private HttpServlet servlet;

    public HttpServlet getServlet() {
        return servlet;
    }

    public void setBaseServlet(HttpServlet servlet) {
        this.servlet = servlet;
    }

    @Before
    public void setUp() throws Exception {
        ServletConfig servletConfig = mock(ServletConfig.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(request.getServletContext()).thenReturn(servletContext);
        when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);
        when(request.getSession()).thenReturn(httpSession);
        servlet.init(servletConfig);
    }
}

package ua.nure.apukhtin.hospital.captcha;

import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Vlad on 25.01.2017.
 */
public class CaptchaManager {

    private static final int TIMEOUT = 3000;
    private static final String CHECK_URL = "https://www.google.com/recaptcha/api/siteverify";
    private static final Pattern CHECK_PATTERN = Pattern.compile("\"?success\"?:\\s*+\"?(true|false)\"?");

    private static final String SECRET_PARAM = "secret";
    private static final String RESPONSE_PARAM = "response";


    private String secredKey = "6Lf3GxMUAAAAAMSXkl45TWFI7vfabAKh25yQdbkD";

    public CaptchaManager(String secredKey) {
        //this.secredKey = secredKey;
    }

    public boolean isValid(String captchaResponse) throws IOException {
        String resultJson =
                Request.Post(CHECK_URL)
                        .bodyForm(Form.form().add(SECRET_PARAM, secredKey).add(RESPONSE_PARAM, captchaResponse).build())
                        .connectTimeout(TIMEOUT)
                        .socketTimeout(TIMEOUT)
                        .execute().returnContent().asString();

        return resultFromJson(resultJson);
    }

    private boolean resultFromJson(String string) throws IOException {
        Matcher matcher = CHECK_PATTERN.matcher(string);
        if (matcher.find()) {
            String isSuccessful = matcher.group(1);
            return Boolean.valueOf(isSuccessful);
        } else {
            throw new IOException("Captcha validation response in not valid");
        }
    }

}

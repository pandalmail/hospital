package ua.nure.apukhtin.hospital.listener; /**
 * Created by Vlad on 11.01.2017.
 */

import ua.nure.apukhtin.hospital.config.ApplicationBooster;
import ua.nure.apukhtin.hospital.config.ApplicationConfig;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener()
public class AppInitializerListener implements ServletContextListener, HttpSessionListener {

    // todo fix path
    private static final String APPLICATION_PROPERTIES_LOCATION = "WEB-INF/classes/application.properties";
    private ApplicationBooster applicationBooster;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        ApplicationConfig appConfig = ApplicationConfig
                .builder()
                .fromProperties(servletContext.getRealPath(APPLICATION_PROPERTIES_LOCATION))
                .build();
        applicationBooster = new ApplicationBooster(appConfig, servletContext);
        applicationBooster.boost();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

    }
}

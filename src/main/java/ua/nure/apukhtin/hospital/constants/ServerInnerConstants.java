package ua.nure.apukhtin.hospital.constants;

/**
 * Created by Vlad on 10.01.2017.
 */
public class ServerInnerConstants {

    public static final String PAGE_PREFIX = "/WEB-INF/pages/";

    public static final String DOCTORS_BY_CATEGORIES_ATTR = "doctorsByCategories";
    public static final String ERRORS_ATTR = "errors";

    public static final String DOCTOR_SERVICE_ATTR = "doctorService";
    public static final String DOCTOR_CATEGORY_SERVICE_ATTR = "doctorCategoryService";
    public static final String PATIENT_SERVICE_ATTR = "patientService";
    public static final String USER_SERVICE_ATTR = "userService";
    public static final String ILLNESS_PERIOD_ATTR = "illnessPeriodService";
    public static final String APPLICATION_CONFIG_ATTR = "applicationConfig";
    public static final String PATIENTS_ATTR = "patients";
    public static final String USER_ATTR = "user";
    public static final String PATIENT_ATTR = "patient";
    public static final String MAX_PATIENT_PAGE_ATTR = "maxPatientsPage";
    public static final String PRESCRIPTION_SERVICE_ATTR = "prescriptionService";
    public static final String SUCCESS_MSG_ATTR = "successMsg";
    public static final String REPORT_MANAGER_ATTR = "reportManager";
    public static final String NURSE_SERVICE_ATTR = "nurseServiceAttr";

    public static final String CATEGORY_ERROR_NAME_KEY = "admin.section.error.categories.name";
    public static final String CATEGORY_ERROR_DESCR_KEY = "admin.section.error.categories.descr";

    public static final String PATIENT_ID_PARAM = "pid";
    public static final String DOCTOR_ID_PARAM = "docId";
    public static final String ILLNESS_PERIOD_ID_PARAM = "period_id";
    public static final String PATIENT_PAGE_PARAM = "patientPage";
    public static final String CATEGORY_PARAM = "category";
    public static final String ILLNESS_PERIOD_PAGE_PARAM = "periodsPage";

    public static final String HOME_URL = "/home";

    public static final String ADMIN_ROLE = "admin";
    public static final String DOCTOR_ROLE = "doctor";
    public static final String PATIENT_ROLE = "patient";
    public static final String NURSE_ROLE = "nurse";

    public static final String NURSES = "nurses";
    public static final String CAPTCHA_PARAM = "g-recaptcha-response";
    public static final String CAPTCHA_MANAGER_ATTR = "CAPTCHA_MANAGER_ATTR";
}

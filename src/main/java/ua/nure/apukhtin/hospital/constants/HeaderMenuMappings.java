package ua.nure.apukhtin.hospital.constants;

import java.util.*;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;

/**
 * Created by Vlad on 16.01.2017.
 */
public class HeaderMenuMappings {

    private static final Map<String, Mappings> USER_MAPPINGS = new HashMap<>();

    static {
        defineMappings();
    }

    private static void defineMappings() {
        Mappings adminMappings = new Mappings();
        adminMappings.add("header.add.doctor", "/admin/add/doctor");
        adminMappings.add("header.add.patient", "/admin/add/patient");

        // yet empty -> for further project expanding
        Mappings nurseMappings = new Mappings();
        Mappings doctorMappings = new Mappings();
        Mappings patientMappings = new Mappings();

        USER_MAPPINGS.put(ADMIN_ROLE, adminMappings);
        USER_MAPPINGS.put(NURSE_ROLE, nurseMappings);
        USER_MAPPINGS.put(DOCTOR_ROLE, doctorMappings);
        USER_MAPPINGS.put(PATIENT_ROLE, patientMappings);
    }

    public Mappings forRole(String role) {
        switch (role) {
            case "admin":
                return USER_MAPPINGS.get("admin");
            case "patient":
                return USER_MAPPINGS.get("patient");
            case "nurse":
                return USER_MAPPINGS.get("nurse");
            case "doctor":
                return USER_MAPPINGS.get("doctor");
            default:
                // empty
                return new Mappings();
        }
    }

    public static class Mappings {
        private Map<String, String> values = new LinkedHashMap<>();

        public void add(String name, String url) {
            values.put(name, url);
        }

        public Map<String, String> getAll() {
            return new LinkedHashMap<>(values);
        }
    }
}

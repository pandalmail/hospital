package ua.nure.apukhtin.hospital.transaction;

import org.apache.log4j.Logger;
import ua.nure.apukhtin.hospital.dao.DAO;
import ua.nure.apukhtin.hospital.dao.helper.JdbcCloser;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Vlad on 12.01.2017.
 */
public class TransactionManager {

    private static Logger logger = Logger.getLogger(TransactionManager.class);
    private DataSource dataSource;

    public TransactionManager(DataSource dataSource) {
        logger.debug("Instantiating transaction with datasource: " + dataSource);
        this.dataSource = Objects.requireNonNull(dataSource);
    }

    public <T> T inTransaction(TransactionAction<T> action, DAO... daos) throws TransactionException {
        logger.info("Beginning transaction");
        Connection connection = null;
        try {
            logger.debug("Get a connection from datasource: " + dataSource);
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            logger.debug("Set one connection to all daos given: " + Arrays.toString(daos));
            for (DAO dao : daos) {
                dao.setConnection(connection);
            }

            logger.info("Executing user transaction actions");
            T result = action.execute();
            logger.info("Try to commit changes");
            connection.commit();

            logger.info("Changes commited successfully. Returning results if any");
            return result;
        } catch (Exception e) {
            logger.info("Rolling back the transaction");
            rollback(connection);
            throw new TransactionException(e);
        } finally {
            logger.debug("Clear daos from custom set connections");
            // Avoid memory leaks in ThreadLocal within servlets multithreading medium
            for (DAO dao : daos) {
                dao.unsetConnection();
            }
            close(connection);
        }
    }

    private void close(Connection connection) throws TransactionException {
        try {
            JdbcCloser.close(connection);
        } catch (SQLException e) {
            throw new TransactionException(e);
        }
    }

    private void rollback(Connection connection) throws TransactionException {
        logger.debug("Trying to rollback connection");
        logger.debug(connection);
        try {
            if (connection != null && !connection.isClosed()) {
                connection.rollback();
                logger.info("Transaction has been rolled back successfully");
            } else {
                logger.info("Transaction has NOT been rolled back because connection is null or already closed");
            }
        } catch (SQLException e) {
            logger.warn("Exception while rolling back the transaction", e);
            throw new TransactionException(e);
        }
    }



}

package ua.nure.apukhtin.hospital.transaction;

/**
 * Created by Vlad on 12.01.2017.
 */
public interface TransactionAction<T> {
    T execute() throws Exception;
}

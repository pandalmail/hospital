package ua.nure.apukhtin.hospital.tag;

import ua.nure.apukhtin.hospital.constants.HeaderMenuMappings;
import ua.nure.apukhtin.hospital.constants.HeaderMenuMappings.Mappings;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.Map;

/**
 * Created by Vlad on 16.01.2017.
 */
public class DynamicHeaderMenuTag extends SimpleTagSupport {

    private static final String ROW_TEMPLATE = "<li><a href=\"%s\"><fmt:message key=\"%s\"/></a></li>%n";
    private static HeaderMenuMappings headerMenuMappings = new HeaderMenuMappings();

    private String var;

    @Override
    public void doTag() throws JspException, IOException {
        PageContext page = ((PageContext) getJspContext());
        JspWriter out = getJspContext().getOut();
        Mappings mappings = mappingsFromRequest(((HttpServletRequest) page.getRequest()));
        Map<String, String> allUrls = mappings.getAll();
        page.setAttribute(var, allUrls);
    }

    private Mappings mappingsFromRequest(HttpServletRequest request) {
        if (request.isUserInRole("admin")) {
            return headerMenuMappings.forRole("admin");
        } else if (request.isUserInRole("nurse")) {
            return headerMenuMappings.forRole("nurse");
        } else if (request.isUserInRole("doctor")) {
            return headerMenuMappings.forRole("doctor");
        } else if (request.isUserInRole("patient")) {
            return headerMenuMappings.forRole("patient");
        } else {
            return new Mappings();
        }
    }

    public String getVar() {
        return var;
    }

    public void setVar(String var) {
        this.var = var;
    }
}

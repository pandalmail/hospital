package ua.nure.apukhtin.hospital.config.builder;

import ua.nure.apukhtin.hospital.config.ApplicationConfig;

/**
 * Created by Vlad on 11.01.2017.
 */
public interface ApplicationBuilder {

    ApplicationConfig build();

}

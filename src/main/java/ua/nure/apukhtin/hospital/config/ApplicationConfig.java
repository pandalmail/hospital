package ua.nure.apukhtin.hospital.config;

import ua.nure.apukhtin.hospital.config.builder.ApplicationBuilder;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.Properties;

/**
 * Created by Vlad on 10.01.2017.
 */
// todo write logs
public class ApplicationConfig {

    private String passwordDigest;
    private String passwordEncoding;
    private SimpleDateFormat dateFormat;
    private int pageSize;

    private String reportOutPath;
    private String reportTemplatePath;

    private ApplicationConfig(String passwordDigest,
                              String passwordEncoding,
                              SimpleDateFormat simpleDateFormat,
                              int pageSize,
                              String reportOutPath,
                              String reportTemplatePath) {
        this.passwordDigest = passwordDigest;
        this.passwordEncoding = passwordEncoding;
        dateFormat = simpleDateFormat;
        this.pageSize = pageSize;
        this.reportOutPath = reportOutPath;
        this.reportTemplatePath = reportTemplatePath;
    }

    public static ApplicationConfigBuilder builder() {
        return new ApplicationConfigBuilder();
    }

    public int getPageSize() {
        return pageSize;
    }

    public String getPasswordDigest() {
        return passwordDigest;
    }

    public String getPasswordEncoding() {
        return passwordEncoding;
    }

    public SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public String getReportOutPath() {
        return reportOutPath;
    }

    public String getReportTemplatePath() {
        return reportTemplatePath;
    }

    public static class ApplicationConfigBuilder implements ApplicationBuilder {

        /**
         * Default values
         **/
        private String passwordDigest = "MD5";
        private String passwordEncoding = "UTF-8";
        private String dateFormat = "yyyy-MM-dd";
        private String reportOutPath = "/";
        private String reportTemplatePath = "WEB-INF/classes/hospital-report.jasper";

        private int pageSize = 10;

        public ApplicationConfigBuilder fromProperties(Properties properties) {
            Optional<String> passwordProp = Optional.ofNullable(properties.getProperty("user.password.digest"));
            Optional<String> encodingProp = Optional.ofNullable(properties.getProperty("user.password.encoding"));
            Optional<String> dateFormatProp = Optional.ofNullable(properties.getProperty("application.date.format"));
            Optional<String> pageSizeProp = Optional.ofNullable(properties.getProperty("application.page.size"));
            Optional<String> reportTemplateLocationProp =
                    Optional.ofNullable(properties.getProperty("report.location.template"));
            Optional<String> reportOutLocationProp = Optional.ofNullable(properties.getProperty("report.location.out"));

            passwordDigest = passwordProp.orElse(passwordDigest);
            passwordEncoding = encodingProp.orElse(passwordEncoding);
            dateFormat = dateFormatProp.orElse(dateFormat);
            pageSize = pageSizeProp
                    .map(Integer::parseInt)
                    .orElse(pageSize);
            reportOutPath = reportOutLocationProp.orElse(reportOutPath);
            reportTemplatePath = reportTemplateLocationProp.orElse(reportTemplatePath);

            return this;
        }

        public ApplicationConfigBuilder fromProperties(String propertiesFile) {
            Properties properties = new Properties();
            try {

                properties.load(new FileInputStream(propertiesFile));
            } catch (IOException e) {
                // if some exceptions occur -> load default configs
                return this;
            }
            return fromProperties(properties);
        }

        public ApplicationConfigBuilder setPasswordDigest(String passwordDigest) {
            this.passwordDigest = passwordDigest;
            return this;
        }

        public ApplicationConfigBuilder setPasswordEncoding(String passwordEncoding) {
            this.passwordEncoding = passwordEncoding;
            return this;
        }

        public ApplicationConfigBuilder setDateFormat(String dateFormat) {
            this.dateFormat = dateFormat;
            return this;
        }

        public ApplicationConfigBuilder setPageSize(int pageSize) {
            this.pageSize = pageSize;
            return this;
        }

        @Override
        public ApplicationConfig build() {
            return new ApplicationConfig(
                    passwordDigest,
                    passwordEncoding,
                    new SimpleDateFormat(dateFormat),
                    pageSize, reportOutPath, reportTemplatePath);
        }
    }
}

package ua.nure.apukhtin.hospital.config;

import ua.nure.apukhtin.hospital.captcha.CaptchaManager;
import ua.nure.apukhtin.hospital.dao.*;
import ua.nure.apukhtin.hospital.dao.impl.*;
import ua.nure.apukhtin.hospital.report.ReportManager;
import ua.nure.apukhtin.hospital.service.*;
import ua.nure.apukhtin.hospital.service.impl.*;
import ua.nure.apukhtin.hospital.transaction.TransactionManager;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.sql.DataSource;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;

/**
 * Created by Vlad on 11.01.2017.
 */
public class ApplicationBooster {

    private static final String DATA_SOURCE_NAME = "java:comp/env/jdbc/applicationDB";

    private ApplicationConfig applicationConfig;
    private ServletContext servletContext;

    public ApplicationBooster(ApplicationConfig applicationConfig, ServletContext servletContext) {
        this.applicationConfig = applicationConfig;
        this.servletContext = servletContext;
    }

    public void boost() {
        bindServices();
        bindOther();
    }

    private void bindOther() {
        String reportDestinationPath = servletContext.getRealPath(applicationConfig.getReportOutPath());
        String templateReportPath = servletContext.getRealPath(applicationConfig.getReportTemplatePath());

        ReportManager reportManager =
                new ReportManager(applicationConfig, reportDestinationPath, templateReportPath);
        servletContext.setAttribute(REPORT_MANAGER_ATTR, reportManager);
        //-- capthca
        servletContext.setAttribute(CAPTCHA_MANAGER_ATTR, new CaptchaManager("foo"));

    }

    private void bindServices() {
        DoctorDAO doctorDAO = new DoctorDAOImpl();
        DoctorCategoryDAO doctorCategoryDAO = new DoctorCategoryDAOImpl();
        PatientDAO patientDao = new PatientDAOImpl();
        UserDAO userDAO = new UserDAOImpl();
        IllnessPeriodDAO illnessPeriodDAO = new IllnessPeriodDAOImpl();
        DiagnosisDAO diagnosisDAO = new DiagnosisDAOImpl(illnessPeriodDAO);
        PrescriptionsDAO prescriptionsDAO = new PrescriptionsDAOImpl();
        NurseDAO nurseDAO = new NurseDAOImpl();

        DataSource dataSource = null;

        try {
            dataSource = (DataSource) new InitialContext().lookup(DATA_SOURCE_NAME);
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
        TransactionManager transactionManager = new TransactionManager(dataSource);

        DoctorCategoryService doctorCategoryService = doctorCategoryService = new DoctorCategoryServiceImpl(doctorCategoryDAO, transactionManager);
        PatientService patientService = patientService = new PatientServiceImpl(transactionManager, patientDao, applicationConfig, doctorDAO, illnessPeriodDAO);
        UserService userService = userService = new UserServiceImpl(transactionManager, userDAO);
        IllnessPeriodService illnessPeriodService = illnessPeriodService = new IllnessPeriodServiceImpl(transactionManager, illnessPeriodDAO, applicationConfig, diagnosisDAO, patientDao);
        // todo doctorservice is too large of dependencies
        DoctorService doctorService = doctorService = new DoctorServiceImpl(transactionManager, doctorDAO, doctorCategoryDAO, applicationConfig, prescriptionsDAO, diagnosisDAO, illnessPeriodService, patientDao);
        PrescriptionService prescriptionService = new PrescriptionServiceImpl(transactionManager, prescriptionsDAO);
        NurseService nurseService = new NurseServiceImpl(transactionManager, nurseDAO, applicationConfig);

        servletContext.setAttribute(DOCTOR_SERVICE_ATTR, doctorService);
        servletContext.setAttribute(DOCTOR_CATEGORY_SERVICE_ATTR, doctorCategoryService);
        servletContext.setAttribute(PATIENT_SERVICE_ATTR, patientService);
        servletContext.setAttribute(APPLICATION_CONFIG_ATTR, applicationConfig);
        servletContext.setAttribute(USER_SERVICE_ATTR, userService);
        servletContext.setAttribute(PRESCRIPTION_SERVICE_ATTR, prescriptionService);
        servletContext.setAttribute(ILLNESS_PERIOD_ATTR, illnessPeriodService);
        servletContext.setAttribute(NURSE_SERVICE_ATTR, nurseService);
    }
}

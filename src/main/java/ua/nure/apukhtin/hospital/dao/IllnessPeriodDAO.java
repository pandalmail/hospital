package ua.nure.apukhtin.hospital.dao;

import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.IllnessPeriod;

import java.util.Date;
import java.util.List;

/**
 * Created by Vlad on 20.01.2017.
 */
public interface IllnessPeriodDAO extends DAO {

    default boolean hasLastOpened(long patientId) throws DAOException {
        return getLastOpened(patientId) != null;
    }

    IllnessPeriod create(IllnessPeriod entity, long patientId) throws DAOException;

    IllnessPeriod getLastOpened(long patientId) throws DAOException;

    List<IllnessPeriod> getWithLimit(long offset, long count, long patientId) throws DAOException;

    long count(long patientId) throws DAOException;

    void closeWithDate(long illnessPeriodId, Date date, long finalDiagnosisId) throws DAOException;

    IllnessPeriod getLast(Long patientId) throws DAOException;
}

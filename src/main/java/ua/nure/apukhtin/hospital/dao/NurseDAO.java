package ua.nure.apukhtin.hospital.dao;

import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.Nurse;

import java.util.List;

/**
 * Created by Vlad on 24.01.2017.
 */
public interface NurseDAO extends DAO {
    Nurse create(Nurse nurse) throws DAOException;

    List<Nurse> getAll() throws DAOException;
}

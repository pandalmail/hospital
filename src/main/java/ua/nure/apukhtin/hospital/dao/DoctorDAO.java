package ua.nure.apukhtin.hospital.dao;

import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.Doctor;
import ua.nure.apukhtin.hospital.model.Patient;

import java.util.List;

/**
 * Created by Vlad on 12.01.2017.
 */
public interface DoctorDAO extends CrudDAO<Doctor, Long> {

    List<Patient> getWithDoctor(long docId, int startWith, int offset) throws DAOException;

}

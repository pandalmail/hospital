package ua.nure.apukhtin.hospital.dao.impl;

import ua.nure.apukhtin.hospital.dao.PatientDAO;
import ua.nure.apukhtin.hospital.dao.SimpleDAO;
import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.Doctor;
import ua.nure.apukhtin.hospital.model.Patient;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Vlad on 15.01.2017.
 */
public class PatientDAOImpl extends SimpleDAO implements PatientDAO {

    private static final String CREATE_PATIENT_USER_PARTIAL_SQL
            = "INSERT INTO USERS(LOGIN, PASSWORD, NAME) VALUES (?,?,?) ";
    private static final String CREATE_PATIENT_PARTIAL_SQL = "INSERT INTO PATIENTS(ID, BIRTH_DATE, DOCTOR_ID) VALUES (?,?,?) ";
    private static final String INSERT_PATIENT_ROLE = "INSERT INTO USER_ROLES(login, role) VALUES(?,?)";

    private static final String READ_ALL_PATIENTS_SQL = "SELECT USERS.ID, USERS.LOGIN, USERS.NAME,PATIENTS.BIRTH_DATE," +
            " PATIENTS.IS_DISCHARGED, PATIENTS.DOCTOR_ID, docusers.NAME " +
            "FROM PATIENTS JOIN USERS ON USERS.ID = PATIENTS.ID " +
            "LEFT JOIN USERS docusers ON PATIENTS.DOCTOR_ID = docusers.ID ";

    private static final String USER_WHERE_CLAUSE_SQL = "WHERE USERS.ID=? ";
    private static final String ASSIGN_WITH_DOC_SQL =
            "UPDATE PATIENTS SET DOCTOR_ID = ? WHERE ID = ?";
    private static final String GET_COUNT_SQL = "SELECT count(*) FROM PATIENTS";
    private static final String DISCHARGE_SQL = "UPDATE PATIENTS SET IS_DISCHARGED = ? WHERE ID = ?";

    @Override
    public Patient create(Patient entity) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = getConnection();
            statement = connection.prepareStatement(CREATE_PATIENT_USER_PARTIAL_SQL,
                    PreparedStatement.RETURN_GENERATED_KEYS);
            statement.setString(1, entity.getLogin());
            statement.setString(2, entity.getPassword());
            statement.setString(3, entity.getName());
            statement.executeUpdate();

            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                long id = resultSet.getLong(1);
                entity.setId(id);
            }

            close(statement);
            statement = connection.prepareStatement(INSERT_PATIENT_ROLE);
            statement.setString(1, entity.getLogin());
            statement.setString(2, entity.getRole());


            Date sqlDate = new Date(entity.getBirthDate().getTime());
            Optional<Long> docId = Optional.of(entity)
                    .map(Patient::getDoctor)
                    .map(Doctor::getId);

            close(statement);
            statement = connection.prepareStatement(CREATE_PATIENT_PARTIAL_SQL);
            statement.setLong(1, entity.getId());
            statement.setDate(2, sqlDate);
            if (!docId.isPresent()) {
                statement.setNull(3, Types.BIGINT);
            } else {
                statement.setLong(3, docId.get());
            }
            statement.executeUpdate();

            return entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            close(resultSet);
        }
    }

    @Override
    public List<Patient> getWithLimit(long offset, long count) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Patient> patients = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(READ_ALL_PATIENTS_SQL + OFFSET_CONSTRAINT_SQL);
            preparedStatement.setLong(1, offset);
            preparedStatement.setLong(2, count);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Patient patient = fromResultSet(resultSet);
                patients.add(patient);
            }

            return patients;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
            close(resultSet);
        }

    }

    @Override
    public Patient read(Long primaryKey) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Patient patient = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(READ_ALL_PATIENTS_SQL + USER_WHERE_CLAUSE_SQL);
            preparedStatement.setLong(1, primaryKey);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                patient = fromResultSet(resultSet);
            }

            return patient;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
            close(resultSet);
        }
    }

    private Patient fromResultSet(ResultSet resultSet) throws SQLException {
        Patient patient = new Patient();
        patient.setId(resultSet.getLong(1));
        patient.setLogin(resultSet.getString(2));
        patient.setName(resultSet.getString(3));
        patient.setBirthDate(resultSet.getDate(4));
        patient.setDischarged(resultSet.getBoolean(5));

        Doctor doctor = new Doctor();
        doctor.setId(resultSet.getLong(6));
        doctor.setName(resultSet.getString(7));
        patient.setDoctor(doctor);

        return patient;
    }

    @Override
    public boolean update(Patient entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(Long primaryKey) throws DAOException {
        return false;
    }

    @Override
    public List<Patient> getAll() throws DAOException {
        return null;
    }

    @Override
    public void assignWithDoctor(Long docId, Long uId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(ASSIGN_WITH_DOC_SQL);
            preparedStatement.setLong(1, docId);
            preparedStatement.setLong(2, uId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
        }

    }

    @Override
    public long count() throws DAOException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GET_COUNT_SQL);
            if (resultSet.next()) {
                return resultSet.getLong(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            close(resultSet);
        }
    }

    @Override
    public void setDischarged(long patientId, boolean isDischarged) throws DAOException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = getConnection().prepareStatement(DISCHARGE_SQL);
            preparedStatement.setBoolean(1, isDischarged);
            preparedStatement.setLong(2, patientId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
        }
    }
}

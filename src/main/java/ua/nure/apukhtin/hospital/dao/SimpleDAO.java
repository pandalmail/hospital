package ua.nure.apukhtin.hospital.dao;

import org.apache.log4j.Logger;
import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Vlad on 12.01.2017.
 */
public abstract class SimpleDAO implements DAO {

    protected static final String OFFSET_CONSTRAINT_SQL = "OFFSET ? ROWS FETCH FIRST ? ROWS ONLY ";
    private static Logger logger = Logger.getLogger(SimpleDAO.class);
    private ThreadLocal<Connection> localConnection = new ThreadLocal<>();

    protected static void close(Statement statement) throws DAOException {
        logger.debug("Closing statement: " + statement);
        try {
            if (statement != null && !statement.isClosed()) {
                statement.close();
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    protected static void close(ResultSet resultSet) throws DAOException {
        logger.debug("Closing resultSet: " + resultSet);
        try {
            if (resultSet != null && !resultSet.isClosed()) {
                resultSet.close();
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    protected Connection getConnection() {
        return localConnection.get();
    }

    @Override
    public void setConnection(Connection localConnection) {
        this.localConnection.set(localConnection);
    }

    @Override
    public void unsetConnection() {
        this.localConnection.remove();
    }
}

package ua.nure.apukhtin.hospital.dao.impl;

import org.apache.log4j.Logger;
import ua.nure.apukhtin.hospital.dao.DoctorDAO;
import ua.nure.apukhtin.hospital.dao.SimpleDAO;
import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.Doctor;
import ua.nure.apukhtin.hospital.model.Patient;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.DOCTOR_ROLE;

/**
 * Created by Vlad on 12.01.2017.
 */
public class DoctorDAOImpl extends SimpleDAO implements DoctorDAO {

    private static final String INSERT_DOC = "INSERT INTO DOCTORS(ID, CATEGORY) VALUES (?, ?)";
    private static final String INSERT_DOC_PARTIAL_USERS_SQL = "INSERT INTO USERS(LOGIN, PASSWORD, NAME) VALUES(?,?,?)";
    private static final String INSERT_DOC_ROLE = "INSERT INTO USER_ROLES(login, role) VALUES(?,?)";
    private static final String READ_DOCTOR_SQL =
            "SELECT USERS.ID, LOGIN, PASSWORD, USERS.NAME, DOCTOR_CATEGORY.NAME, COUNT(PATIENTS.ID) " +
                    "FROM USERS " +
                    "  JOIN DOCTORS ON USERS.ID = DOCTORS.ID JOIN DOCTOR_CATEGORY " +
                    "  ON DOCTORS.CATEGORY = DOCTOR_CATEGORY.NAME " +
                    "  LEFT JOIN PATIENTS ON DOCTORS.ID = PATIENTS.DOCTOR_ID " +
                    "  WHERE DOCTORS.ID = ? " +
                    "GROUP BY USERS.ID, LOGIN, PASSWORD, USERS.NAME, DOCTOR_CATEGORY.NAME ";
    private static final String READ_ALL_SQL =
            "SELECT USERS.ID, LOGIN, PASSWORD, USERS.NAME, DOCTOR_CATEGORY.NAME, COUNT(PATIENTS.ID)" +
                    "FROM USERS " +
                    "  JOIN DOCTORS ON USERS.ID = DOCTORS.ID JOIN DOCTOR_CATEGORY " +
                    "  ON DOCTORS.CATEGORY = DOCTOR_CATEGORY.NAME " +
                    "  LEFT JOIN PATIENTS ON DOCTORS.ID = PATIENTS.DOCTOR_ID " +
                    "GROUP BY USERS.ID, LOGIN, PASSWORD, USERS.NAME, DOCTOR_CATEGORY.NAME ";
    private static final String GET_PATIENTS_WITH_DOCID_SQL =
            "SELECT USERS.ID, LOGIN, USERS.NAME, PATIENTS.BIRTH_DATE " +
                    "FROM USERS " +
                    "JOIN PATIENTS ON USERS.ID = PATIENTS.ID " +
                    "WHERE PATIENTS.DOCTOR_ID = ? ";

    private static final String GET_BY_LOGIN_SQL =
            "SELECT USERS.ID, LOGIN, PASSWORD, USERS.NAME, DOCTOR_CATEGORY.NAME " +
                    "FROM USERS " +
                    "  JOIN DOCTORS ON USERS.ID = DOCTORS.ID JOIN DOCTOR_CATEGORY " +
                    "  ON DOCTORS.CATEGORY = DOCTOR_CATEGORY.NAME " +
                    "  LEFT JOIN PATIENTS ON DOCTORS.ID = PATIENTS.DOCTOR_ID " +
                    "  WHERE USERS.LOGIN = ? ";

    private static Logger logger = Logger.getLogger(DoctorDAOImpl.class);

    @Override
    public Doctor create(Doctor entity) throws DAOException {
        logger.info("Beginning creating doctor: " + entity);
        PreparedStatement anotherStatement = null;
        PreparedStatement preparedStatement = null;
        ResultSet generatedKeys = null;
        Connection connection = null;
        try {
            connection = getConnection();
            logger.debug("Got connection: " + connection);
            long id = 0;
            preparedStatement = connection.prepareStatement(INSERT_DOC_PARTIAL_USERS_SQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getLogin());
            preparedStatement.setString(2, entity.getPassword());
            preparedStatement.setString(3, entity.getName());
            logger.debug("Executing query: " + INSERT_DOC_PARTIAL_USERS_SQL);
            preparedStatement.executeUpdate();
            logger.debug("executeUpdate() successful");
            generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
                logger.debug("got doctor index: " + id);
            }

            logger.debug("Executing another query: " + INSERT_DOC);
            anotherStatement = connection.prepareStatement(INSERT_DOC);
            anotherStatement.setLong(1, id);
            anotherStatement.setString(2, entity.getCategory());
            anotherStatement.executeUpdate();
            logger.debug("executeUpdate() successful");
            close(anotherStatement);

            anotherStatement = connection.prepareStatement(INSERT_DOC_ROLE);
            anotherStatement.setString(1, entity.getLogin());
            anotherStatement.setString(2, DOCTOR_ROLE);
            anotherStatement.executeUpdate();

            entity.setId(id);
            logger.info("Doctor has been created sucessfully. Returning entity with id: " + id);
            return entity;

        } catch (SQLException e) {
            logger.warn("Exception has occurred while creating doctor", e);
            throw new DAOException(e);
        } finally {
            logger.info("Closing JDBC resources");
            close(generatedKeys);
            close(preparedStatement);
            close(anotherStatement);
        }
    }

    @Override
    public Doctor read(Long primaryKey) throws DAOException {
        logger.info("Reading doctor from storage with id " + primaryKey);
        Connection connection = null;
        PreparedStatement statement = null;
        Doctor result = null;
        ResultSet resultSet = null;
        try {
            connection = getConnection();
            logger.debug("Got connection: " + connection);
            statement = connection.prepareStatement(READ_DOCTOR_SQL);
            statement.setLong(1, primaryKey);

            logger.debug("Executing query: " + READ_DOCTOR_SQL);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = new Doctor();
                result = docFromResultSet(resultSet);
            }

            logger.info("Doctor has been extracted -> Returning " + result);
            return result;
        } catch (SQLException e) {
            logger.warn("Exception has occurred while creating doctor", e);
            throw new DAOException(e);
        } finally {
            logger.info("Closing JDBC resources");
            close(resultSet);
            close(statement);
        }
    }

    @Override
    public boolean update(Doctor entity) throws DAOException {
        try {
            throw new SQLFeatureNotSupportedException();
        } catch (SQLFeatureNotSupportedException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean delete(Long primaryKey) throws DAOException {
        try {
            throw new SQLFeatureNotSupportedException();
        } catch (SQLFeatureNotSupportedException e) {
            throw new DAOException(e);
        }

    }

    @Override
    public List<Patient> getWithDoctor(long docId, int startWith, int offset) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Patient> patients = new ArrayList<>();

        try {
            connection = getConnection();
            statement = connection.prepareStatement(GET_PATIENTS_WITH_DOCID_SQL + OFFSET_CONSTRAINT_SQL);
            statement.setLong(1, docId);
            statement.setInt(2, startWith);
            statement.setInt(3, offset);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Patient patient = cutPatientFromResultSet(resultSet);
                patients.add(patient);
            }

            return patients;
        } catch (SQLException e) {
            logger.warn("Exception has occurred while creating doctor", e);
            throw new DAOException(e);
        } finally {
            logger.info("Closing JDBC resources");
            close(statement);
            close(resultSet);
        }
    }

    @Override
    public List<Doctor> getAll() throws DAOException {
        logger.info("Getting all doctors");
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<Doctor> doctors = new ArrayList<>();

        try {
            connection = getConnection();
            logger.debug("Got connection: " + connection);
            statement = connection.createStatement();
            logger.debug("Executing query: " + READ_ALL_SQL);
            resultSet = statement.executeQuery(READ_ALL_SQL);

            while (resultSet.next()) {
                doctors.add(docFromResultSet(resultSet));
            }
            logger.info("Doctors have been successfully extracted from storage. Size: " + doctors.size());
            return doctors;
        } catch (SQLException e) {
            logger.warn("Exception has occurred while creating doctor", e);
            throw new DAOException(e);
        } finally {
            logger.info("Closing JDBC resources");
            close(statement);
            close(resultSet);
        }
    }

    private Doctor docFromResultSet(ResultSet resultSet) throws SQLException {
        return docFromResultSet(resultSet, true);
    }

    private Doctor docFromResultSet(ResultSet resultSet, boolean withPatientCount)
            throws SQLException {
        Doctor doc = new Doctor();
        doc.setId(resultSet.getLong(1));
        doc.setLogin(resultSet.getString(2));
        doc.setPassword(resultSet.getString(3));
        doc.setName(resultSet.getString(4));
        doc.setCategory(resultSet.getString(5));
        if (withPatientCount) {
            doc.setPatientsCount(resultSet.getInt(6));
        }
        return doc;
    }

    private Patient cutPatientFromResultSet(ResultSet resultSet) throws SQLException {
        Patient patient = new Patient();
        patient.setId(resultSet.getLong(1));
        patient.setLogin(resultSet.getString(2));
        patient.setName(resultSet.getString(3));
        patient.setBirthDate(resultSet.getDate(4));
        return patient;
    }


}




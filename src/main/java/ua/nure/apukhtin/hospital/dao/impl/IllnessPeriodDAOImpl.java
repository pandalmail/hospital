package ua.nure.apukhtin.hospital.dao.impl;

import ua.nure.apukhtin.hospital.dao.IllnessPeriodDAO;
import ua.nure.apukhtin.hospital.dao.SimpleDAO;
import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.IllnessPeriod;
import ua.nure.apukhtin.hospital.model.Patient;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static java.sql.PreparedStatement.RETURN_GENERATED_KEYS;

/**
 * Created by Vlad on 20.01.2017.
 */
public class IllnessPeriodDAOImpl extends SimpleDAO implements IllnessPeriodDAO {

    private static final String GET_OPENED_FOR_USER_SQL =
            "SELECT * FROM ILLNESS_PERIODS WHERE PATIENT_ID = ? AND END_TIME IS NULL";
    private static final String CREATE_ILLNESS_PERIOD_SQL =
            "INSERT INTO ILLNESS_PERIODS(START_TIME, PATIENT_ID) VALUES (?,?)";

    private static final String GET_ALL_FOR_PATIENT_SQL =
            "SELECT * FROM ILLNESS_PERIODS WHERE PATIENT_ID=? ORDER BY END_TIME DESC NULLS FIRST ";

    private static final String GET_COUNT =
            "SELECT count(*) FROM ILLNESS_PERIODS WHERE PATIENT_ID=? ";

    private static final String CLOSE_ILLNESS_PERIOD
            = "UPDATE ILLNESS_PERIODS SET END_TIME = ?, FINAL_DIAGNOSIS_ID = ? WHERE ID = ?";
    private static final String GET_FOR_USER_SQL =
            "SELECT * FROM ILLNESS_PERIODS WHERE PATIENT_ID = ? ORDER BY END_TIME DESC  ";

    @Override
    public IllnessPeriod create(IllnessPeriod entity, long patientId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(CREATE_ILLNESS_PERIOD_SQL, RETURN_GENERATED_KEYS);
            preparedStatement.setDate(1, new Date(entity.getStartDate().getTime()));
            preparedStatement.setLong(2, patientId);
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                Long id = resultSet.getLong(1);
                entity.setId(id);
            }

            return entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
            close(resultSet);
        }
    }


    @Override
    public void closeWithDate(long illnessPeriodId, java.util.Date date, long finalDiagnosisId) throws DAOException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = getConnection().prepareStatement(CLOSE_ILLNESS_PERIOD);
            preparedStatement.setDate(1, new Date(date.getTime()));
            preparedStatement.setLong(2, finalDiagnosisId);
            preparedStatement.setLong(3, illnessPeriodId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
        }
    }

    @Override
    public IllnessPeriod getLast(Long patientId) throws DAOException {
        return queryForPatient(GET_FOR_USER_SQL, patientId);
    }

    @Override
    public IllnessPeriod getLastOpened(long patientId) throws DAOException {
        return queryForPatient(GET_OPENED_FOR_USER_SQL, patientId);
    }

    @Override
    public List<IllnessPeriod> getWithLimit(long offset, long count, long patientId) throws DAOException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<IllnessPeriod> result = new LinkedList<>();
        try {
            Connection connection = getConnection();
            preparedStatement = connection.prepareStatement(GET_ALL_FOR_PATIENT_SQL + OFFSET_CONSTRAINT_SQL);
            preparedStatement.setLong(1, patientId);
            preparedStatement.setLong(2, offset);
            preparedStatement.setLong(3, count);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                IllnessPeriod illnessPeriod = getFromResultSet(resultSet);
                result.add(illnessPeriod);
            }

            return result;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
            close(resultSet);
        }
    }

    @Override
    public long count(long patientId) throws DAOException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        long count = 0;

        try {
            Connection connection = getConnection();
            preparedStatement = connection.prepareStatement(GET_COUNT);
            preparedStatement.setLong(1, patientId);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                count = resultSet.getLong(1);
            }

            return count;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
            close(resultSet);
        }

    }

    private IllnessPeriod queryForPatient(String statement, long patientId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        IllnessPeriod illnessPeriod = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setLong(1, patientId);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                illnessPeriod = getFromResultSet(resultSet);
            }
            return illnessPeriod;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
            close(resultSet);
        }

    }

    private IllnessPeriod getFromResultSet(ResultSet resultSet) throws SQLException {
        IllnessPeriod illnessPeriod = new IllnessPeriod();
        illnessPeriod.setId(resultSet.getLong(1));
        illnessPeriod.setStartDate(resultSet.getDate(2));
        illnessPeriod.setEndDate(resultSet.getDate(3));

        Patient patient = new Patient();
        patient.setId(resultSet.getLong(4));
        return illnessPeriod;
    }

}

package ua.nure.apukhtin.hospital.dao.impl;

import ua.nure.apukhtin.hospital.dao.DiagnosisDAO;
import ua.nure.apukhtin.hospital.dao.IllnessPeriodDAO;
import ua.nure.apukhtin.hospital.dao.SimpleDAO;
import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.Diagnosis;
import ua.nure.apukhtin.hospital.model.IllnessPeriod;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Vlad on 21.01.2017.
 */
public class DiagnosisDAOImpl extends SimpleDAO implements DiagnosisDAO {

    private static final String READ_DIAGNOSIS =
            "SELECT ID, NAME, DETECTED_DOCTOR, ILLNESS_PERIOD_ID FROM DIAGNOSIS WHERE ID = ? ";
    private static final String CREATE_DIAGNOSIS_SQL =
            "INSERT INTO DIAGNOSIS(NAME, DETECTED_DOCTOR, ILLNESS_PERIOD_ID) VALUES (?,?,?) ";
    private static final String GET_ASSOCIATED_DIAGNOSIS
            = "SELECT * FROM DIAGNOSIS WHERE ILLNESS_PERIOD_ID = ? ORDER BY ID DESC ";
    private static final String GET_FINAL_ILLNESS_DIAGNOSIS
            = "SELECT * FROM DIAGNOSIS WHERE DIAGNOSIS.ID IN (" +
            "SELECT FINAL_DIAGNOSIS_ID FROM ILLNESS_PERIODS WHERE ILLNESS_PERIODS.ID = ?" +
            ") ";
    private IllnessPeriodDAO illnessPeriodDAO;

    public DiagnosisDAOImpl(IllnessPeriodDAO illnessPeriodDAO) {
        this.illnessPeriodDAO = Objects.requireNonNull(illnessPeriodDAO);
    }

    @Override
    public void setConnection(Connection localConnection) {
        super.setConnection(localConnection);
        illnessPeriodDAO.setConnection(localConnection);
    }

    @Override
    public void unsetConnection() {
        super.unsetConnection();
        illnessPeriodDAO.unsetConnection();
    }

    @Override
    public Diagnosis createForIllnessPeriod(Diagnosis entity, IllnessPeriod illnessPeriod, long patientId, long docId) throws DAOException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            long illnessPeriodId = illnessPeriod.getId();

            preparedStatement = connection.prepareStatement(CREATE_DIAGNOSIS_SQL, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setLong(2, docId);
            preparedStatement.setLong(3, illnessPeriodId);

            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                Long id = resultSet.getLong(1);
                entity.setId(id);
            }
            return entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(resultSet);
            close(preparedStatement);
        }
    }

    @Override
    public Diagnosis read(long id) throws DAOException {
        return queryWith(id, READ_DIAGNOSIS);
    }

    @Override
    public List<Diagnosis> getIllnessPeriodDiagnosis(long illnessPeriodId) throws DAOException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Diagnosis> result = new LinkedList<>();
        try {
            Connection connection = getConnection();
            preparedStatement = connection.prepareStatement(GET_ASSOCIATED_DIAGNOSIS);
            preparedStatement.setLong(1, illnessPeriodId);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Diagnosis diagnosis = diagnosisFromResultSet(resultSet);
                result.add(diagnosis);
            }
            return result;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
            close(resultSet);
        }
    }

    @Override
    public Diagnosis getIllnessPeriodFinalDiagnosis(long id) throws DAOException {
        return queryWith(id, GET_FINAL_ILLNESS_DIAGNOSIS);
    }

    private Diagnosis queryWith(long id, String statement) throws DAOException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Diagnosis result = null;
        try {
            preparedStatement = getConnection().prepareStatement(statement);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = diagnosisFromResultSet(resultSet);
            }
            return result;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
            close(resultSet);
        }

    }

    private Diagnosis diagnosisFromResultSet(ResultSet resultSet) throws SQLException {
        Diagnosis diagnosis = new Diagnosis();
        diagnosis.setId(resultSet.getLong(1));
        diagnosis.setName(resultSet.getString(2));
        return diagnosis;
    }

}

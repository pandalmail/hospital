package ua.nure.apukhtin.hospital.dao;

import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.Prescriptions;

/**
 * Created by Vlad on 21.01.2017.
 */
public interface PrescriptionsDAO extends DAO {

    void createForDiagnosis(Prescriptions prescriptions, Long diagnosisId) throws DAOException;

    Prescriptions getPrescriptionsFor(long diagnosisId) throws DAOException;

    void makeDrugsDone(long prescriptionId) throws DAOException;

    void makeOperationsDone(long prescriptionId) throws DAOException;

    void makeProceduresDone(long prescriptionId) throws DAOException;

}

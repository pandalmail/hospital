package ua.nure.apukhtin.hospital.dao.impl;

import org.apache.log4j.Logger;
import ua.nure.apukhtin.hospital.dao.DoctorCategoryDAO;
import ua.nure.apukhtin.hospital.dao.SimpleDAO;
import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 15.01.2017.
 */
public class DoctorCategoryDAOImpl extends SimpleDAO implements DoctorCategoryDAO {

    private static final String NEW_CATEGORY_SQL = "INSERT INTO DOCTOR_CATEGORY(name) VALUES (?)";
    private static final String READ_ALL_SQL = "SELECT NAME FROM DOCTOR_CATEGORY";
    private static Logger logger = Logger.getLogger(DoctorCategoryDAOImpl.class);


    @Override
    public String create(String category) throws DAOException {
        logger.info("Beginning creating category");
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            logger.debug("Got connection: " + connection);
            statement = connection.prepareStatement(NEW_CATEGORY_SQL);
            logger.debug("Executing query: " + NEW_CATEGORY_SQL);
            statement.setString(1, category);
            statement.executeUpdate();
            return category;
        } catch (SQLException e) {
            logger.warn("Exception has occurred while creating doctor", e);
            throw new DAOException(e);
        } finally {
            logger.info("Closing JDBC resources");
            close(statement);
        }
    }

    @Override
    public String read(String primaryKey) throws DAOException {
        return null;
    }

    @Override
    public boolean update(String entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(String primaryKey) throws DAOException {
        return false;
    }

    @Override
    public List<String> getAll() throws DAOException {
        logger.info("Getting all categories");
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<String> categories = new ArrayList<>();

        try {
            connection = getConnection();
            logger.debug("Got connection: " + connection);
            statement = connection.createStatement();
            logger.debug("Executing query: " + READ_ALL_SQL);
            resultSet = statement.executeQuery(READ_ALL_SQL);

            while (resultSet.next()) {
                String category = resultSet.getString(1);
                categories.add(category);
            }
            logger.info("Categories have been successfully extracted from storage. Size: " + categories.size());
            return categories;
        } catch (SQLException e) {
            logger.warn("Exception has occurred while getting categories", e);
            throw new DAOException(e);
        } finally {
            logger.info("Closing JDBC resources");
            close(statement);
            close(resultSet);
        }

    }
}

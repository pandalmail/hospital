package ua.nure.apukhtin.hospital.dao;

import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.Patient;

import java.util.List;

/**
 * Created by Vlad on 15.01.2017.
 */
public interface PatientDAO extends CrudDAO<Patient, Long> {

    List<Patient> getWithLimit(long offset, long count) throws DAOException;

    void assignWithDoctor(Long docId, Long uId) throws DAOException;

    long count() throws DAOException;

    void setDischarged(long patientId, boolean isDischarged) throws DAOException;
}

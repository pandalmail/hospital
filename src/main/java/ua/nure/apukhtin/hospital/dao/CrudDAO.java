package ua.nure.apukhtin.hospital.dao;

import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;

import java.util.List;

/**
 * Created by Vlad on 12.01.2017.
 */
public interface CrudDAO<T, PK> extends DAO {

    T create(T entity) throws DAOException;

    T read(PK primaryKey) throws DAOException;

    boolean update(T entity) throws DAOException;

    boolean delete(PK primaryKey) throws DAOException;

    List<T> getAll() throws DAOException;

    default boolean exists(PK primaryKey) throws DAOException {
        return read(primaryKey) != null;
    }
}

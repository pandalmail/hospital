package ua.nure.apukhtin.hospital.dao.impl;

import ua.nure.apukhtin.hospital.dao.PrescriptionsDAO;
import ua.nure.apukhtin.hospital.dao.SimpleDAO;
import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.Prescription;
import ua.nure.apukhtin.hospital.model.Prescriptions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Vlad on 21.01.2017.
 */
public class PrescriptionsDAOImpl extends SimpleDAO implements PrescriptionsDAO {

    private static final String GET_PROCEDURES_SQL =
            "SELECT ID, NAME, DONE FROM PROCEDURES WHERE PRESCRIPTION_ID IN ( " +
                    " SELECT ID FROM PRESCRIPTION WHERE DIAGNOZIS_ID = ?) ";
    private static final String GET_DRUGS_SQL =
            "SELECT ID, NAME, DONE FROM DRUGS WHERE PRESCRIPTION_ID IN ( " +
                    " SELECT ID FROM PRESCRIPTION WHERE DIAGNOZIS_ID = ?) ";
    private static final String GET_OPERATIONS_SQL =
            "SELECT ID, NAME, DONE FROM OPERATIONS WHERE PRESCRIPTION_ID IN ( " +
                    " SELECT ID FROM PRESCRIPTION WHERE DIAGNOZIS_ID = ?) ";
    private static final String CREATE_PRESCRIPTION_PARTIAL
            = "INSERT INTO PRESCRIPTION(DIAGNOZIS_ID) VALUES (?)";

    // -------
    private static final String CREATE_PRESCRIPTIONS_PROCEDURES_PATRIAL
            = "INSERT INTO PROCEDURES(NAME, PRESCRIPTION_ID) VALUES (?,?)";
    private static final String CREATE_PRESCRIPTIONS_DRUGS_PATRIAL
            = "INSERT INTO DRUGS(NAME, PRESCRIPTION_ID) VALUES (?,?)";
    private static final String CREATE_PRESCRIPTIONS_OPERATIONS_PATRIAL
            = "INSERT INTO OPERATIONS(NAME, PRESCRIPTION_ID) VALUES (?,?)";

    private static final String MARK_DRUGS_DONE =
            "UPDATE DRUGS SET DONE = TRUE WHERE ID = ?";
    private static final String MARK_OPERATIONS_DONE =
            "UPDATE OPERATIONS SET DONE = TRUE WHERE ID = ?";
    private static final String MARK_PROCEDURES_DONE =
            "UPDATE PROCEDURES SET DONE = TRUE WHERE ID = ?";

    // -------
    @Override
    public void createForDiagnosis(Prescriptions prescriptions, Long diagnosisId) throws DAOException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(CREATE_PRESCRIPTION_PARTIAL, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, diagnosisId);
            preparedStatement.executeUpdate();

            long prescriptionId = 0;
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                prescriptionId = generatedKeys.getLong(1);
            }
            close(preparedStatement);

            updatePartial(CREATE_PRESCRIPTIONS_PROCEDURES_PATRIAL, prescriptions.getProcedures(),
                    connection, prescriptionId);
            updatePartial(CREATE_PRESCRIPTIONS_DRUGS_PATRIAL, prescriptions.getDrugs(),
                    connection, prescriptionId);
            updatePartial(CREATE_PRESCRIPTIONS_OPERATIONS_PATRIAL, prescriptions.getOperations(),
                    connection, prescriptionId);

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Prescriptions getPrescriptionsFor(long diagnosisId) throws DAOException {
        Prescriptions prescriptions = new Prescriptions();
        try {
            Connection connection = getConnection();
            queryPartial(GET_PROCEDURES_SQL, prescriptions.getProcedures(), connection, diagnosisId);
            queryPartial(GET_DRUGS_SQL, prescriptions.getDrugs(), connection, diagnosisId);
            queryPartial(GET_OPERATIONS_SQL, prescriptions.getOperations(), connection, diagnosisId);

            return prescriptions;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void makeDrugsDone(long prescriptionId) throws DAOException {
        partialDone(MARK_DRUGS_DONE, prescriptionId);
    }

    @Override
    public void makeOperationsDone(long prescriptionId) throws DAOException {
        partialDone(MARK_OPERATIONS_DONE, prescriptionId);

    }

    @Override
    public void makeProceduresDone(long prescriptionId) throws DAOException {
        partialDone(MARK_PROCEDURES_DONE, prescriptionId);
    }

    private void partialDone(String statementString, long prescriptionId) throws DAOException {
        PreparedStatement preparedStatement = null;
        try {
            Connection connection = getConnection();
            preparedStatement = connection.prepareStatement(statementString);
            preparedStatement.setLong(1, prescriptionId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(preparedStatement);
        }

    }

    private void queryPartial(String statementString, List<Prescription> partialElements,
                              Connection connection, long diagnosisId)
            throws SQLException, DAOException {
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(statementString);
            preparedStatement.setLong(1, diagnosisId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Prescription prescription = prescriptionFromResultSet(resultSet);
                partialElements.add(prescription);
            }
        } finally {
            close(preparedStatement);
            close(resultSet);
        }

    }

    private Prescription prescriptionFromResultSet(ResultSet resultSet) throws SQLException {
        Prescription prescription = new Prescription();
        prescription.setId(resultSet.getLong(1));
        prescription.setName(resultSet.getString(2));
        prescription.setDone(resultSet.getBoolean(3));

        return prescription;
    }

    private void updatePartial(String statementString, List<Prescription> partialElements,
                               Connection connection, Long prescriptionId) throws SQLException, DAOException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(statementString);
            for (Prescription partialElement : partialElements) {
                preparedStatement.setString(1, partialElement.getName());
                preparedStatement.setLong(2, prescriptionId);
                preparedStatement.executeUpdate();
            }
        } finally {
            close(preparedStatement);
        }
    }

}

package ua.nure.apukhtin.hospital.dao;

import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.Diagnosis;
import ua.nure.apukhtin.hospital.model.IllnessPeriod;

import java.util.List;

/**
 * Created by Vlad on 21.01.2017.
 */
public interface DiagnosisDAO extends DAO {

    Diagnosis createForIllnessPeriod(Diagnosis entity, IllnessPeriod illnessPeriod, long patientId, long docId) throws DAOException;

    Diagnosis read(long id) throws DAOException;

    List<Diagnosis> getIllnessPeriodDiagnosis(long illnessPeriodId) throws DAOException;

    Diagnosis getIllnessPeriodFinalDiagnosis(long id) throws DAOException;
}

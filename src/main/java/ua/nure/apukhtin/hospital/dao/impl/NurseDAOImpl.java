package ua.nure.apukhtin.hospital.dao.impl;

import ua.nure.apukhtin.hospital.dao.NurseDAO;
import ua.nure.apukhtin.hospital.dao.SimpleDAO;
import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.Nurse;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.NURSE_ROLE;

/**
 * Created by Vlad on 24.01.2017.
 */
public class NurseDAOImpl extends SimpleDAO implements NurseDAO {

    private static final String INSERT_NURSE = "INSERT INTO NURSES(ID) VALUES (?)";
    private static final String INSERT_NURSE_PARTIAL_USERS_SQL = "INSERT INTO USERS(LOGIN, PASSWORD, NAME) VALUES(?,?,?)";
    private static final String INSERT_NURSE_ROLE = "INSERT INTO USER_ROLES(login, role) VALUES(?,?)";
    private static final String READ_ALL_SQL =
            "SELECT USERS.ID, LOGIN, USERS.NAME " +
                    "FROM USERS RIGHT JOIN NURSES ON USERS.ID = NURSES.ID";

    @Override
    public Nurse create(Nurse entity) throws DAOException {
        PreparedStatement anotherStatement = null;
        PreparedStatement preparedStatement = null;
        ResultSet generatedKeys = null;
        Connection connection = null;
        try {
            connection = getConnection();
            long id = 0;
            preparedStatement = connection.prepareStatement(INSERT_NURSE_PARTIAL_USERS_SQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getLogin());
            preparedStatement.setString(2, entity.getPassword());
            preparedStatement.setString(3, entity.getName());
            preparedStatement.executeUpdate();
            generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
            }

            anotherStatement = connection.prepareStatement(INSERT_NURSE);
            anotherStatement.setLong(1, id);
            anotherStatement.executeUpdate();
            close(anotherStatement);

            anotherStatement = connection.prepareStatement(INSERT_NURSE_ROLE);
            anotherStatement.setString(1, entity.getLogin());
            anotherStatement.setString(2, NURSE_ROLE);
            anotherStatement.executeUpdate();

            entity.setId(id);
            return entity;

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(generatedKeys);
            close(preparedStatement);
            close(anotherStatement);
        }

    }

    @Override
    public List<Nurse> getAll() throws DAOException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<Nurse> nurses = new ArrayList<>();

        try {
            connection = getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(READ_ALL_SQL);

            while (resultSet.next()) {
                nurses.add(fromResultSet(resultSet));
            }
            return nurses;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(statement);
            close(resultSet);
        }
    }

    private Nurse fromResultSet(ResultSet resultSet) throws SQLException {
        Nurse nurse = new Nurse();
        nurse.setId(resultSet.getLong(1));
        nurse.setLogin(resultSet.getString(2));
        nurse.setName(resultSet.getString(3));
        return nurse;
    }
}

package ua.nure.apukhtin.hospital.dao.impl;

import org.apache.log4j.Logger;
import ua.nure.apukhtin.hospital.dao.SimpleDAO;
import ua.nure.apukhtin.hospital.dao.UserDAO;
import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Vlad on 19.01.2017.
 */
public class UserDAOImpl extends SimpleDAO implements UserDAO {

    private static final String GET_BY_LOGIN_SQL =
            "SELECT users.ID, users.LOGIN, users.PASSWORD, users.NAME, roles.ROLE " +
                    "FROM USERS users JOIN USER_ROLES roles ON users.LOGIN=roles.LOGIN " +
                    "WHERE users.LOGIN = ?";

    private static Logger logger = Logger.getLogger(UserDAOImpl.class);

    @Override
    public User getByLogin(String login) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            connection = getConnection();
            logger.debug("Got connection: " + connection);
            statement = connection.prepareStatement(GET_BY_LOGIN_SQL);
            statement.setString(1, login);
            logger.debug("Executing query: " + GET_BY_LOGIN_SQL);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                user = fromResultSet(resultSet);
            }
            return user;
        } catch (SQLException e) {
            logger.warn("Exception has occurred while creating user", e);
            throw new DAOException(e);
        } finally {
            logger.info("Closing JDBC resources");
            close(statement);
            close(resultSet);
        }

    }

    private User fromResultSet(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong(1));
        user.setLogin(resultSet.getString(2));
        user.setPassword(resultSet.getString(3));
        user.setName(resultSet.getString(4));
        user.setRole(resultSet.getString(5));
        return user;
    }
}

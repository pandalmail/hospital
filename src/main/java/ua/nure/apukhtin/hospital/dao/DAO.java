package ua.nure.apukhtin.hospital.dao;

import java.sql.Connection;

/**
 * Created by Vlad on 12.01.2017.
 */
public interface DAO {
    void setConnection(Connection localConnection);

    void unsetConnection();
}

package ua.nure.apukhtin.hospital.dao;

import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.User;

/**
 * Created by Vlad on 19.01.2017.
 */
public interface UserDAO extends DAO {

    User getByLogin(String login) throws DAOException;

}

package ua.nure.apukhtin.hospital.model;

import java.text.Collator;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.PATIENT_ROLE;

/**
 * Created by Vlad on 11.01.2017.
 */
public class Patient extends User {

    public static final Comparator<Patient> NAME_COMPARATOR =
            Comparator.comparing(Patient::getName, Collator.getInstance()::compare);
    public static final Comparator<Patient> DATE_COMPARATOR = Comparator.comparing(Patient::getBirthDate);

    public static final Comparator<Patient> DEFAULT_COMPARATOR = NAME_COMPARATOR;

    private Date birthDate;
    private Doctor doctor;
    private boolean discharged; 
    
    public Patient() {
        setRole(PATIENT_ROLE);
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        if (!super.equals(o)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(birthDate, patient.birthDate) &&
                Objects.equals(doctor, patient.doctor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), birthDate, doctor);
    }

    public boolean isDischarged() {
        return discharged;
    }

    public void setDischarged(boolean discharged) {
        this.discharged = discharged;
    }
}

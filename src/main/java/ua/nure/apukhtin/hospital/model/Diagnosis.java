package ua.nure.apukhtin.hospital.model;

import java.util.Objects;

/**
 * Created by Vlad on 21.01.2017.
 */
public class Diagnosis {

    private long id;
    private String name;

    public Diagnosis() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Diagnosis)) {
            return false;
        }
        Diagnosis diagnosis = (Diagnosis) o;
        return id == diagnosis.id &&
                Objects.equals(name, diagnosis.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}

package ua.nure.apukhtin.hospital.model;

import java.util.Objects;

/**
 * Created by Vlad on 22.01.2017.
 */
public class Prescription {

    private long id;
    private String name;
    private boolean done;

    public Prescription() {
    }

    public Prescription(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Prescription)) {
            return false;
        }
        Prescription that = (Prescription) o;
        return id == that.id &&
                done == that.done &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, done);
    }

}

package ua.nure.apukhtin.hospital.model;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.NURSE_ROLE;

/**
 * Created by Vlad on 24.01.2017.
 */
public class Nurse extends User {

    public Nurse() {
        super.setRole(NURSE_ROLE);
    }
}
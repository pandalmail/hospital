package ua.nure.apukhtin.hospital.model;

import java.text.Collator;
import java.util.Comparator;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.DOCTOR_ROLE;

/**
 * Created by Vlad on 11.01.2017.
 */
public class Doctor extends User {

    public static final Comparator<Doctor> NAME_COMPARATOR =
            Comparator.comparing(Doctor::getName, Collator.getInstance()::compare);
    public static final Comparator<Doctor> CATEGORY_COMPARATOR
            = Comparator.comparing(Doctor::getCategory, Collator.getInstance()::compare);

    public static final Comparator<Doctor> PATIENT_COUNT_COMPARATOR = Comparator.comparing(Doctor::getPatientsCount).reversed();
    public static final Comparator<Doctor> DEFAULT_COMPARATOR = Comparator.comparing(doctor -> 0);


    private String category;
    private int patientsCount;

    public Doctor() {
        super.setRole(DOCTOR_ROLE);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPatientsCount() {
        return patientsCount;
    }

    public void setPatientsCount(int patientsCount) {
        this.patientsCount = patientsCount;
    }


}

package ua.nure.apukhtin.hospital.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 21.01.2017.
 */
public class Prescriptions {

    long id;
    private List<Prescription> procedures = new ArrayList<>();
    private List<Prescription> drugs = new ArrayList<>();
    private List<Prescription> operations = new ArrayList<>();

    public Prescriptions() {
    }

    public List<Prescription> getProcedures() {
        return procedures;
    }

    public List<Prescription> getDrugs() {
        return drugs;
    }

    public List<Prescription> getOperations() {
        return operations;
    }
}

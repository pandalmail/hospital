package ua.nure.apukhtin.hospital.model;

import java.util.Date;
import java.util.Objects;

/**
 * Created by Vlad on 20.01.2017.
 */
public class IllnessPeriod {

    private Long id;
    private String name;
    private Date startDate;
    private Date endDate;
    private String finalDiagnosis;

    public IllnessPeriod() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getFinalDiagnosis() {
        return finalDiagnosis;
    }

    public void setFinalDiagnosis(String finalDiagnosis) {
        this.finalDiagnosis = finalDiagnosis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IllnessPeriod)) return false;
        IllnessPeriod that = (IllnessPeriod) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(finalDiagnosis, that.finalDiagnosis);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, startDate, endDate, finalDiagnosis);
    }
}

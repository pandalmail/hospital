package ua.nure.apukhtin.hospital.dto;

import ua.nure.apukhtin.hospital.model.Diagnosis;
import ua.nure.apukhtin.hospital.model.IllnessPeriod;
import ua.nure.apukhtin.hospital.model.Prescriptions;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vlad on 22.01.2017.
 */
public class IllnessPeriodDTO {

    private IllnessPeriod illnessPeriod;
    private Map<Diagnosis, Prescriptions> diagnoses = new LinkedHashMap<>();

    public IllnessPeriodDTO(IllnessPeriod illnessPeriod, List<Diagnosis> diagnoses, List<Prescriptions> diagnosesPrescriptions) {
        this.illnessPeriod = illnessPeriod;

        for (int i = 0; i < diagnoses.size(); i++) {
            Diagnosis diagnosis = diagnoses.get(i);
            Prescriptions prescriptions = diagnosesPrescriptions.get(i);
            this.diagnoses.put(diagnosis, prescriptions);
        }

    }

    public IllnessPeriod getIllnessPeriod() {
        return illnessPeriod;
    }

    public void setIllnessPeriod(IllnessPeriod illnessPeriod) {
        this.illnessPeriod = illnessPeriod;
    }

    public Map<Diagnosis, Prescriptions> getDiagnoses() {
        return diagnoses;
    }
}

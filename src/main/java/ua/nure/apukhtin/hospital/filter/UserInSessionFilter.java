package ua.nure.apukhtin.hospital.filter;

import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.service.UserService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.USER_ATTR;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.USER_SERVICE_ATTR;

/**
 * Created by Vlad on 18.01.2017.
 */
public class UserInSessionFilter implements Filter {

    private UserService userService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        userService = (UserService) filterConfig.getServletContext().getAttribute(USER_SERVICE_ATTR);
        Objects.requireNonNull(userService);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) servletRequest;
        String remoteUser = httpReq.getRemoteUser();

        // if authenticated
        try {
            if (remoteUser != null) {
                Object user = null;
                user = userService.getByLogin(remoteUser);
                httpReq.getSession().setAttribute(USER_ATTR, user);
            }
        } catch (ServiceException e) {
            // no op
        } finally {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
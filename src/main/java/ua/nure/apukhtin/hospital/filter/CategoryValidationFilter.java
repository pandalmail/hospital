package ua.nure.apukhtin.hospital.filter;

import ua.nure.apukhtin.hospital.validation.FormValidationCommand;
import ua.nure.apukhtin.hospital.validation.ValidationException;
import ua.nure.apukhtin.hospital.validation.ValidatorFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.HOME_URL;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.error;

/**
 * Created by Vlad on 15.01.2017.
 */
public class CategoryValidationFilter implements Filter {

    private static final String HTTP_METHOD = "post";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResp = (HttpServletResponse) servletResponse;

        if (HTTP_METHOD.equalsIgnoreCase(httpReq.getMethod())) {
            FormValidationCommand strategy = ValidatorFactory.fromRequest(servletRequest);
            if (strategy != null) {
                try {
                    strategy.validate();
                    filterChain.doFilter(servletRequest, servletResponse);
                } catch (ValidationException e) {
                    error(httpReq, httpResp, HOME_URL, e.getErrors());
                }

            }
        }
    }

    @Override
    public void destroy() {

    }
}

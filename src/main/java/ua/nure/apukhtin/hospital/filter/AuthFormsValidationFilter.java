package ua.nure.apukhtin.hospital.filter;

import org.apache.log4j.Logger;
import ua.nure.apukhtin.hospital.validation.FormValidationCommand;
import ua.nure.apukhtin.hospital.validation.ValidationException;
import ua.nure.apukhtin.hospital.validation.ValidatorFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.ERRORS_ATTR;

/**
 * Created by Vlad on 10.01.2017.
 */
public class AuthFormsValidationFilter implements Filter {

    private static final String HTTP_METHOD_VALIDATING = "post";
    private static final String LOGIN_ATTR = "login";
    private static final String NAME_ATTR = "name";
    private static final String PASSWORD_ATTR = "password";
    private static Logger logger = Logger.getLogger(AuthFormsValidationFilter.class);

    @Override
    public void destroy() {
        logger.trace("filter destroying");

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
        HttpServletRequest httpReq = (HttpServletRequest) req;
        HttpServletResponse httpResp = ((HttpServletResponse) resp);

        storeTemporalValues(httpReq);

        if (!httpReq.getMethod().equalsIgnoreCase(HTTP_METHOD_VALIDATING)) {
            chain.doFilter(req, resp);
            return;
        }

        logger.info("Extracting validator from request");
        FormValidationCommand validator = ValidatorFactory.fromRequest(req);
        try {
            if (validator != null) {
                logger.info("Begin validation");
                validator.validate();
            }
            logger.info("Continuing servlet chain");
            chain.doFilter(req, resp);
        } catch (ValidationException e) {
            logger.debug(String.format("Adding to session attribute[%s] errors[%s]",
                    ERRORS_ATTR, e.getErrors()));
            // todo replace with ServletUtils#error
            httpReq.getSession().setAttribute(ERRORS_ATTR, e.getErrors());

            String requestURI = ((HttpServletRequest) req).getRequestURI();
            logger.info(String.format("Redirecting to requesting url[%s]", requestURI));
            httpResp.sendRedirect(requestURI);
        }
    }

    private void storeTemporalValues(HttpServletRequest request) {
        request.getSession().setAttribute(LOGIN_ATTR, request.getParameter(LOGIN_ATTR));
        request.getSession().setAttribute(NAME_ATTR, request.getParameter(NAME_ATTR));
        request.getSession().setAttribute(PASSWORD_ATTR, request.getParameter(PASSWORD_ATTR));
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        logger.trace("Filter init with " + config);
    }

}

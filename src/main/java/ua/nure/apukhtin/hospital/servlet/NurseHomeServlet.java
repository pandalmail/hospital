package ua.nure.apukhtin.hospital.servlet;

import ua.nure.apukhtin.hospital.model.Patient;
import ua.nure.apukhtin.hospital.service.PatientService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.servlet.utils.ServletUtils;
import ua.nure.apukhtin.hospital.servlet.utils.SortingUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.forward;

/**
 * Created by Vlad on 25.01.2017.
 */
@WebServlet(name = "NurseHomeServlet", urlPatterns = "/nurse")
public class NurseHomeServlet extends HttpServlet {

    private static final String DESTINATION = PAGE_PREFIX + "nurses-dashboard.jsp";

    private PatientService patientService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        patientService = (PatientService) config.getServletContext().getAttribute(PATIENT_SERVICE_ATTR);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        List<Patient> patients = null;
        long currentPage = ServletUtils.getPageForParam(PATIENT_PAGE_PARAM, req);
        long maxPatientPage = 1;
        Comparator<Patient> patientComparator = SortingUtils.patientComparatorFromRequest(req);
        try {
           patients =  patientService.getPatients(currentPage, patientComparator);
        } catch (ServiceException e) {
            errors.put("patients", "error.obtain.descr");
        }

        try {
            maxPatientPage = patientService.getMaxPageCount();
        } catch (ServiceException e) {
            errors.put("doctor.error.patients", "error.obtain.count.descr");
        }

        req.setAttribute(PATIENTS_ATTR, patients);
        req.setAttribute(MAX_PATIENT_PAGE_ATTR, maxPatientPage);
        if (errors.isEmpty()) {
            forward(req, resp, DESTINATION);
        } else {
            forward(req, resp, DESTINATION, errors);
        }
    }
}

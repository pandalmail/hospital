package ua.nure.apukhtin.hospital.servlet;

import ua.nure.apukhtin.hospital.dto.IllnessPeriodDTO;
import ua.nure.apukhtin.hospital.model.Diagnosis;
import ua.nure.apukhtin.hospital.model.IllnessPeriod;
import ua.nure.apukhtin.hospital.model.Patient;
import ua.nure.apukhtin.hospital.model.Prescriptions;
import ua.nure.apukhtin.hospital.service.IllnessPeriodService;
import ua.nure.apukhtin.hospital.service.PatientService;
import ua.nure.apukhtin.hospital.service.PrescriptionService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.forward;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.getPageForParam;

/**
 * Created by Vlad on 20.01.2017.
 */
public class PatientCardServlet extends HttpServlet {

    private static final Pattern PATIENT_ID_PATTERN = Pattern.compile("(patient/card/)(\\d+)(/.+)?");
    private static final String DESTINATION = PAGE_PREFIX + "patient-card.jsp";

    private PatientService patientService;
    private IllnessPeriodService illnessPeriodService;
    private PrescriptionService prescriptionService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        patientService = (PatientService) config.getServletContext().getAttribute(PATIENT_SERVICE_ATTR);
        illnessPeriodService = (IllnessPeriodService) config.getServletContext().getAttribute(ILLNESS_PERIOD_ATTR);
        prescriptionService = (PrescriptionService) config.getServletContext().getAttribute(PRESCRIPTION_SERVICE_ATTR);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        long maxPage = 1;
        long currentPage = getPageForParam(ILLNESS_PERIOD_PAGE_PARAM, req);
        Patient patient = null;
        List<IllnessPeriodDTO> illnessPeriodDTOS = null;

        Optional<Long> patientID = patientIdFromURL(req.getRequestURI());
        if (!patientID.isPresent()) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        try {
            patient = patientService.getById(patientID.get());
        } catch (ServiceException e) {
            errors.put("patient", "error.obtain.descr");
        }

        try {
            maxPage = illnessPeriodService.getMaxPageCount(patientID.get());
        } catch (ServiceException e) {
            errors.put("illnessPeriod", "error.obtain.count.descr");
        }

        try {
            List<IllnessPeriod> illnessPeriods = illnessPeriodService.getForPage(currentPage, patientID.get());
            illnessPeriodDTOS = generatePeriodsDtos(illnessPeriods, errors);
        } catch (ServiceException e) {
            errors.put("illnessPeriod", "error.obtain.descr");
        }

        req.setAttribute(PATIENT_ATTR, patient);
        // todo externalise strings
        req.setAttribute("illnessPeriodDTOs", illnessPeriodDTOS);
        req.setAttribute("periodsLastPage", maxPage);

        if (errors.isEmpty()) {
            forward(req, resp, DESTINATION);
        } else {
            forward(req, resp, DESTINATION, errors);
        }
    }

    private List<IllnessPeriodDTO> generatePeriodsDtos(List<IllnessPeriod> illnessPeriods, Map<String, String> errors) {
        List<IllnessPeriodDTO> illnessPeriodDTOS = new LinkedList<>();
        for (IllnessPeriod illnessPeriod : illnessPeriods) {
            try {
                List<Diagnosis> diagnoses = illnessPeriodService.getDiagnosisFor(illnessPeriod.getId());
                List<Prescriptions> diagnosesPrescriptions = getDiagnosisPrescriptions(diagnoses);

                illnessPeriodDTOS.add(new IllnessPeriodDTO(illnessPeriod, diagnoses, diagnosesPrescriptions));
            } catch (ServiceException e) {
                errors.put("illnessPeriod", "illnessPeriod.error.obtain.any");
                illnessPeriodDTOS.add(new IllnessPeriodDTO(illnessPeriod, Collections.emptyList(), Collections.emptyList()));
            }
        }
        return illnessPeriodDTOS;
    }

    private List<Prescriptions> getDiagnosisPrescriptions(List<Diagnosis> diagnoses) throws ServiceException {
        List<Prescriptions> prescriptions = new LinkedList<>();
        for (Diagnosis diagnosis: diagnoses) {
            prescriptions.add(prescriptionService.getForDiagnosis(diagnosis.getId()));
        }
        return prescriptions;
    }

    static Optional<Long> patientIdFromURL(String url) {
        Matcher matcher = PATIENT_ID_PATTERN.matcher(url);
        Optional<Long> result = empty();
        if (matcher.find()) {
            String idString = matcher.group(2);
            result = ofNullable(idString)
                    .filter(value -> !value.isEmpty())
                    .map(Long::valueOf);
        }

        return result;
    }

}

package ua.nure.apukhtin.hospital.servlet;

import ua.nure.apukhtin.hospital.constants.ServerInnerConstants;
import ua.nure.apukhtin.hospital.model.Doctor;
import ua.nure.apukhtin.hospital.service.DoctorCategoryService;
import ua.nure.apukhtin.hospital.service.DoctorService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.service.exception.AlreadyExistsException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.DOCTOR_SERVICE_ATTR;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.PAGE_PREFIX;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.*;

/**
 * Created by Vlad on 13.01.2017.
 */
@WebServlet(name = "AddDoctorServlet", urlPatterns = "/admin/add/doctor")
public class AddDoctorServlet extends HttpServlet {

    private static final String TO = PAGE_PREFIX + "new-doc.jsp";

    private DoctorService doctorService;
    private DoctorCategoryService doctorCategoryService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        doctorService = (DoctorService) config.getServletContext().getAttribute(DOCTOR_SERVICE_ATTR);
        doctorCategoryService = (DoctorCategoryService)
                config.getServletContext().getAttribute(ServerInnerConstants.DOCTOR_CATEGORY_SERVICE_ATTR);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Doctor doctor = doctorFromRequest(request);
        Map<String, String> errors = new HashMap<>();

        try {
            doctorService.addDoctor(doctor);
        } catch (ServiceException e) {
            if (e.getCause() instanceof AlreadyExistsException) {
                errors.put("admin.register.doctor.error.login", "admin.register.error.exists.descr");
            }
            errors.put("error.inner", "error.inner.descr");
        }

        if(errors.isEmpty()) {
            success(request, response, "/home", "admin.register.doctor.success");
        } else {
            error(request, response, request.getRequestURI(), errors);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();

        try {
            List<String> categories = doctorCategoryService.getAll();
            getServletContext().setAttribute("categories", categories);
        } catch (ServiceException e) {
            errors.put("admin.register.doctor.error.categories", "admin.register.doctor.error.categories.descr");
        }

        if (errors.isEmpty()) {
            forward(request, response, TO);
        } else {
            forward(request, response, TO, errors);
        }
    }

    private Doctor doctorFromRequest(HttpServletRequest request) {
        Doctor doctor = new Doctor();
        doctor.setLogin(request.getParameter("login"));
        doctor.setPassword(request.getParameter("password"));
        doctor.setName(request.getParameter("name"));
        doctor.setCategory(request.getParameter("doc-spec"));

        return doctor;
    }
}

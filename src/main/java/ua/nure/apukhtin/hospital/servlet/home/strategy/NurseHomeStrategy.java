package ua.nure.apukhtin.hospital.servlet.home.strategy;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vlad on 25.01.2017.
 */
public class NurseHomeStrategy implements HomeStrategy {

    private static final String DESTINATION = "/nurse";

    @Override
    public void redirectHome(HttpServletResponse response) throws IOException {
        response.sendRedirect(DESTINATION);
    }
}

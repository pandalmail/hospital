package ua.nure.apukhtin.hospital.servlet;

import org.apache.log4j.Logger;
import ua.nure.apukhtin.hospital.model.Doctor;
import ua.nure.apukhtin.hospital.model.Nurse;
import ua.nure.apukhtin.hospital.model.Patient;
import ua.nure.apukhtin.hospital.service.DoctorService;
import ua.nure.apukhtin.hospital.service.NurseService;
import ua.nure.apukhtin.hospital.service.PatientService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Optional.ofNullable;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.forward;
import static ua.nure.apukhtin.hospital.servlet.utils.SortingUtils.doctorComparatorFromRequest;
import static ua.nure.apukhtin.hospital.servlet.utils.SortingUtils.patientComparatorFromRequest;

/**
 * Created by Vlad on 10.01.2017.
 */
@WebServlet(name = "AdminHomeServlet", urlPatterns = "/admin")
public class AdminHomeServlet extends HttpServlet {

    private static final String ADMIN_DASHBOARD_JSP = PAGE_PREFIX + "admin-dashboard.jsp";
    private static final String PATIENT_PAGE = "patientPage";

    private static final int DEFAULT_PAGE = 1;

    private static Logger logger = Logger.getLogger(AdminHomeServlet.class);
    private DoctorService doctorService;
    private PatientService patientService;
    private NurseService nurseService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        logger.debug("Getting DoctorService from request attribute " + DOCTOR_SERVICE_ATTR);
        doctorService = (DoctorService) config.getServletContext().getAttribute(DOCTOR_SERVICE_ATTR);
        logger.debug("DoctorService == null: " + (doctorService == null));
        patientService = (PatientService) config.getServletContext().getAttribute(PATIENT_SERVICE_ATTR);
        nurseService = (NurseService) config.getServletContext().getAttribute(NURSE_SERVICE_ATTR);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        Map<String, List<Doctor>> doctorsByCategories = null;
        Long maxPageCount = null;
        List<Patient> patients = null;
        List<Nurse> nurses = null;
        int page = ofNullable(request.getParameter(PATIENT_PAGE))
                .filter(param -> !param.isEmpty())
                .map(Integer::parseInt)
                .orElse(DEFAULT_PAGE);

        try {
            logger.info("Try to acquire doctors by categories");
            // todo sort and unsort categories
            Comparator<Doctor> doctorComparator = doctorComparatorFromRequest(request);
            doctorsByCategories = doctorService.getDoctorsByCategories(doctorComparator);
            logger.debug("Acquired categories == null: " + (doctorsByCategories == null));
        } catch (ServiceException e) {
            logger.warn("Cannot acquire doctors by categories", e);
            errors.put(CATEGORY_ERROR_NAME_KEY, CATEGORY_ERROR_DESCR_KEY);
        }

        try {
            Comparator<Patient> patientComparator = patientComparatorFromRequest(request);
            patients = patientService.getPatients(page, patientComparator);
        } catch (ServiceException e) {
            errors.put("error.inner", "error.obtain.descr");
        }

        try {
            nurses = nurseService.getAll();
        } catch (ServiceException e) {
            errors.put("nurses", "error.obtain.descr");
        }

        try {
            maxPageCount = patientService.getMaxPageCount();
        } catch (ServiceException e) {
            errors.put("patients", "error.obtain.count.descr");
        }

        logger.info("Binding doctors by categories to attribute " + DOCTORS_BY_CATEGORIES_ATTR);
        request.setAttribute(DOCTORS_BY_CATEGORIES_ATTR, doctorsByCategories);
        request.setAttribute(PATIENTS_ATTR, patients);
        request.setAttribute(MAX_PATIENT_PAGE_ATTR, maxPageCount);
        request.setAttribute(NURSES, nurses);

        if (errors.isEmpty()) {
            logger.info("No errors while requesting have been found. Redirecting to dashboard: " + ADMIN_DASHBOARD_JSP);
            forward(request, response, ADMIN_DASHBOARD_JSP);
        } else {
            logger.warn("There are errors while processing request " + errors.keySet());
            forward(request, response, ADMIN_DASHBOARD_JSP, errors);
        }
    }
}

package ua.nure.apukhtin.hospital.servlet;

import ua.nure.apukhtin.hospital.model.Patient;
import ua.nure.apukhtin.hospital.model.Prescription;
import ua.nure.apukhtin.hospital.model.Prescriptions;
import ua.nure.apukhtin.hospital.model.User;
import ua.nure.apukhtin.hospital.service.DoctorService;
import ua.nure.apukhtin.hospital.service.PatientService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.*;

/**
 * Created by Vlad on 21.01.2017.
 */
@WebServlet(name = "AssignDiagnosisServlet", urlPatterns = "/doctor/assign")
public class AssignDiagnosisServlet extends HttpServlet {

    private static final String DESTINATION = PAGE_PREFIX + "set-diagnosis.jsp";

    private DoctorService doctorService;
    private PatientService patientService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        doctorService = (DoctorService) config.getServletContext().getAttribute(DOCTOR_SERVICE_ATTR);
        patientService = (PatientService) config.getServletContext().getAttribute(PATIENT_SERVICE_ATTR);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        Optional<Long> patientId = getPatientIdFromParam(req);
        Patient patient = null;

        if (!patientId.isPresent()) {
            errors.put("patient", "error.notfound.descr");
        } else {
            try {
                patient = patientService.getById(patientId.get());
            } catch (ServiceException e) {
                errors.put("patient", "error.notfound.descr");
            }
        }

        req.setAttribute(PATIENT_ATTR, patient);
        if (errors.isEmpty()) {
            forward(req, resp, DESTINATION);
        } else {
            String previousURL = req.getHeader("Referer");
            forward(req, resp, HOME_URL, errors);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        Prescriptions prescriptions = prescriptionsFromRequest(req);
        String diagnosisName = req.getParameter("diagnosis");
        Optional<Long> patientId = getPatientIdFromParam(req);
        Patient patient = null;
        User doc = (User) req.getSession().getAttribute(USER_ATTR);

        if (!patientId.isPresent()) {
            errors.put("patient", "error.notfound.descr");
        } else {
            try {
                doctorService.addDiagnosisForPatient(diagnosisName, patientId.get(), doc.getId(), prescriptions);
            } catch (ServiceException e) {
                errors.put("error.inner", "error.inner.descr");
            }
        }

        if (errors.isEmpty()) {
            success(req, resp, "/patient/" + patientId.get(), "doctor.add.diagnosis");
        } else {
            error(req, resp, HOME_URL, errors);
        }
    }

    private Prescriptions prescriptionsFromRequest(HttpServletRequest req) {
        List<Prescription> procedures =
                ofNullable(req.getParameterValues("procedures"))
                        .map(this::stringToPrescription)
                        .orElseGet(ArrayList::new);

        List<Prescription> operations =
                ofNullable(req.getParameterValues("operations"))
                        .map(this::stringToPrescription)
                        .orElseGet(ArrayList::new);

        List<Prescription> drugs =
                ofNullable(req.getParameterValues("drugs"))
                        .map(this::stringToPrescription)
                        .orElseGet(ArrayList::new);

        Prescriptions prescriptions = new Prescriptions();
        prescriptions.getProcedures().addAll(procedures);
        prescriptions.getOperations().addAll(operations);
        prescriptions.getDrugs().addAll(drugs);
        return prescriptions;
    }

    private List<Prescription> stringToPrescription(String[] strings) {
        return Arrays.stream(strings)
                .map(Prescription::new)
                .collect(Collectors.toList());
    }

}


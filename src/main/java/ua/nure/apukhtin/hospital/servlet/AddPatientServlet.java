package ua.nure.apukhtin.hospital.servlet;

import ua.nure.apukhtin.hospital.captcha.CaptchaManager;
import ua.nure.apukhtin.hospital.config.ApplicationConfig;
import ua.nure.apukhtin.hospital.model.Patient;
import ua.nure.apukhtin.hospital.service.PatientService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.*;

/**
 * Created by Vlad on 16.01.2017.
 */

@WebServlet(name = "addPatientServlet", urlPatterns = {"/admin/add/patient", "/register"})
public class AddPatientServlet extends HttpServlet {

    private static final String TO = PAGE_PREFIX + "new-patient.jsp";

    private PatientService patientService;
    private ApplicationConfig applicationConfig;
    private CaptchaManager captchaManager;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        patientService = (PatientService) config.getServletContext().getAttribute(PATIENT_SERVICE_ATTR);
        applicationConfig = (ApplicationConfig) config.getServletContext().getAttribute(APPLICATION_CONFIG_ATTR);
        captchaManager = (CaptchaManager) config.getServletContext().getAttribute(CAPTCHA_MANAGER_ATTR);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forward(req, resp, TO);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        String captcha = req.getParameter(CAPTCHA_PARAM);
        Patient patient = null;

        try {
            if (!captchaManager.isValid(captcha)) {
                errors.put("captcha", "error.captcha.notpass.descr");
            }
        } catch (SocketTimeoutException e) {
            errors.put("captcha", "error.captcha.timeout");
        } catch (IOException e) {
            errors.put("captcha", "error.captcha.io");
        }

        try {
            patient = getPatientFromRequest(req);
        } catch (ParseException e) {
            errors.put("admin.register.patient.error.dateparse", "admin.register.patient.error.dateparse.descr");
        }

        // if captcha failed or user model is not valid already - get back
        if (!errors.isEmpty()) {
            error(req, resp, req.getRequestURI(), errors);
            return;
        }

        try {
            patientService.addPatient(patient);
        } catch (ServiceException e) {
            errors.put("error.inner", "error.inner.descr");
            error(req, resp, req.getRequestURI(), errors);
            return;
        }

        success(req, resp, HOME_URL, "admin.register.patient.success");
    }

    private Patient getPatientFromRequest(HttpServletRequest req) throws ParseException {
        SimpleDateFormat httpFormDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Patient patient = new Patient();

        patient.setLogin(req.getParameter("login"));
        patient.setPassword(req.getParameter("password"));
        patient.setName(req.getParameter("name"));
        patient.setBirthDate(httpFormDateFormat.parse(req.getParameter("birth")));

        return patient;
    }
}

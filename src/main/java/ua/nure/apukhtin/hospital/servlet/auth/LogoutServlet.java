package ua.nure.apukhtin.hospital.servlet.auth;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vlad on 10.01.2017.
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {

    private static Logger logger = Logger.getLogger(LogoutServlet.class);
    private static final String ROOT_LOCATION = "/";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Entering logout request for principal " + req.getUserPrincipal());
        logger.info("Perfoming logout -> refresh principal and role");
        req.logout();
        logger.info("Redirection to root");
        resp.sendRedirect(ROOT_LOCATION);
    }
}

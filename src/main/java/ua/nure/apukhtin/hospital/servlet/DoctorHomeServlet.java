package ua.nure.apukhtin.hospital.servlet;

import ua.nure.apukhtin.hospital.model.Doctor;
import ua.nure.apukhtin.hospital.model.Patient;
import ua.nure.apukhtin.hospital.model.User;
import ua.nure.apukhtin.hospital.service.DoctorService;
import ua.nure.apukhtin.hospital.service.PatientService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.servlet.utils.ServletUtils;
import ua.nure.apukhtin.hospital.servlet.utils.SortingUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.forward;

/**
 * Created by Vlad on 18.01.2017.
 */
@WebServlet(name = "DoctorHomeServlet", value = "/doctor")
public class DoctorHomeServlet extends HttpServlet {

    private static final String DOC_PAGE = PAGE_PREFIX + "doctor-assigned-patients.jsp";
    private DoctorService doctorService;
    private PatientService patientService;
    private Doctor doctor;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        doctorService = (DoctorService) config.getServletContext().getAttribute(DOCTOR_SERVICE_ATTR);
        patientService = (PatientService) config.getServletContext().getAttribute(PATIENT_SERVICE_ATTR);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new LinkedHashMap<>();
        List<Patient> assignedPatients = null;
        long currentPage = ServletUtils.getPageForParam(PATIENT_PAGE_PARAM, req);
        Long maxPageCount = null;
        try {
            long docId = Optional
                    .ofNullable(req.getSession().getAttribute(USER_ATTR))
                    .map(attr -> (User) attr)
                    .map(User::getId)
                    .orElseThrow(IllegalStateException::new);

            Comparator<Patient> patientComparator = SortingUtils.patientComparatorFromRequest(req);
            assignedPatients = doctorService.getAssignedPatients(docId, (int) currentPage, patientComparator);
            req.setAttribute(PATIENTS_ATTR, assignedPatients);
        } catch (IllegalStateException e) {
            errors.put("error.inner", "error.notpresent.descr");
        } catch (ServiceException e) {
            errors.put("doctor.error.patients", "doctor.error.patients.descr");
        }

        try {
            maxPageCount = patientService.getMaxPageCount();
        } catch (ServiceException e) {
            errors.put("patients", "error.obtain.count.descr");
        }

        req.setAttribute(MAX_PATIENT_PAGE_ATTR, maxPageCount);
        req.setAttribute(PATIENT_PAGE_PARAM, currentPage);
        if (errors.isEmpty()) {
            forward(req, resp, DOC_PAGE);
        } else {
            forward(req, resp, DOC_PAGE, errors);
        }
    }
}

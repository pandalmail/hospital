package ua.nure.apukhtin.hospital.servlet;

import org.apache.log4j.Logger;
import ua.nure.apukhtin.hospital.model.Doctor;
import ua.nure.apukhtin.hospital.model.Patient;
import ua.nure.apukhtin.hospital.service.DoctorService;
import ua.nure.apukhtin.hospital.service.PatientService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.service.exception.NotExistsException;
import ua.nure.apukhtin.hospital.servlet.utils.SortingUtils;
import ua.nure.apukhtin.hospital.transaction.TransactionException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.*;

/**
 * Created by Vlad on 19.01.2017.
 */
@WebServlet(name = "AssignPatientServlet", urlPatterns = "/set/doctor")
public class AssignPatientServlet extends HttpServlet {

    private static final String PATIENT_ID_PATAM = "pid";
    private static final String DESTINATION = PAGE_PREFIX + "assign-patient.jsp";
    private static final String DOCTOR_ID_PARAM = "docId";
    private static Logger logger = Logger.getLogger(AssignPatientServlet.class);

    private DoctorService doctorService;
    private PatientService patientService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        doctorService = (DoctorService) config.getServletContext().getAttribute(DOCTOR_SERVICE_ATTR);
        patientService = (PatientService) config.getServletContext().getAttribute(PATIENT_SERVICE_ATTR);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        Map<String, List<Doctor>> doctorsByCategories = null;
        Patient patient = null;

        Optional<Long> patientId = getPatientIdFromParam(req);

        if (!patientId.isPresent()) {
            errors.put("patient.error.empty", "patient.error.empty.descr");
        } else {
            try {
                patient = patientService.getById(patientId.get());
            } catch (ServiceException e) {
                errors.put("patient", "error.notfound.descr");
            }
        }

        try {
            logger.info("Try to acquire doctors by categories");
            // todo sort and unsort categories
            Comparator<Doctor> doctorComparator = SortingUtils.doctorComparatorFromRequest(req);
            doctorsByCategories = doctorService.getDoctorsByCategories(doctorComparator);
            logger.debug("Acquired categories == null: " + (doctorsByCategories == null));
        } catch (ServiceException e) {
            logger.warn("Cannot acquire doctors by categories", e);
            errors.put(CATEGORY_ERROR_NAME_KEY, CATEGORY_ERROR_DESCR_KEY);
        }

        req.setAttribute(DOCTORS_BY_CATEGORIES_ATTR, doctorsByCategories);
        req.setAttribute(PATIENT_ATTR, patient);

        if (errors.isEmpty()) {
            forward(req, resp, DESTINATION);
        } else {
            forward(req, resp, DESTINATION, errors);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new LinkedHashMap<>();
        Optional<Long> patientId = getPatientIdFromParam(req);
        Optional<Long> doctorId = getDocId(req);

        if (!patientId.isPresent()) {
            errors.put("patient", "error.notfound.descr");
        }

        if (!doctorId.isPresent()) {
            errors.put("doctor", "error.notfound.descr");
        }

        if (patientId.isPresent() && doctorId.isPresent()) {
            try {
                patientService.assignWithDoctor(doctorId.get(), patientId.get());
            } catch (ServiceException e) {
                TransactionException transactionException = (TransactionException) e.getCause();
                if (transactionException.getCause() instanceof NotExistsException) {
                    errors.put("doctor-or-patient", "error.notfound.descr");
                } else {
                    errors.put("error.inner", "patient.error.assign.descr");
                }
            }
        }

        if (errors.isEmpty()) {
            success(req, resp, HOME_URL, "patient.assign.success.descr");
        } else {
            error(req, resp, HOME_URL, errors);
        }

    }
}

package ua.nure.apukhtin.hospital.servlet.auth;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vlad on 10.01.2017.
 */
@WebServlet("/unsuccessful-prg-login")
public class UnsuccessfulPRGLoginServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(UnsuccessfulPRGLoginServlet.class);
    private static final String ERROR_LOGIN_PAGE = "/login.jsp?err=true";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Entered PRG error servlet. Redirection to " + ERROR_LOGIN_PAGE);
        response.sendRedirect(ERROR_LOGIN_PAGE);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

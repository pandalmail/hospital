package ua.nure.apukhtin.hospital.servlet;

import ua.nure.apukhtin.hospital.constants.ServerInnerConstants;
import ua.nure.apukhtin.hospital.model.Nurse;
import ua.nure.apukhtin.hospital.service.NurseService;
import ua.nure.apukhtin.hospital.service.exception.AlreadyExistsException;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.*;

/**
 * Created by Vlad on 24.01.2017.
 */
@WebServlet(name = "AddNurseServlet", urlPatterns = "/admin/add/nurse")
public class AddNurseServlet extends HttpServlet {

    private static String DESTINATION = ServerInnerConstants.PAGE_PREFIX + "new-doc.jsp";
    private NurseService nurseService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        nurseService = (NurseService) config.getServletContext().getAttribute(ServerInnerConstants.NURSE_SERVICE_ATTR);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("forNurse", true);
        forward(req, resp, DESTINATION);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Nurse doctor = nurseFromRequest(request);
        Map<String, String> errors = new HashMap<>();

        try {
            nurseService.create(doctor);
        } catch (ServiceException e) {
            // todo already exist to check
            if (e.getCause() instanceof AlreadyExistsException) {
                errors.put("login.login-field", "admin.register.error.exists.descr");
            }
            errors.put("error.inner", "error.inner.descr");
        }

        if(errors.isEmpty()) {
            success(request, response, ServerInnerConstants.HOME_URL, "admin.register.nurse.success");
        } else {
            error(request, response, request.getRequestURI(), errors);
        }
    }

    private Nurse nurseFromRequest(HttpServletRequest request) {
        Nurse nurse = new Nurse();
        //todo externalize
        nurse.setLogin(request.getParameter("login"));
        nurse.setPassword(request.getParameter("password"));
        nurse.setName(request.getParameter("name"));
        return nurse;
    }

}

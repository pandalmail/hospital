package ua.nure.apukhtin.hospital.servlet.utils;

import ua.nure.apukhtin.hospital.model.Doctor;
import ua.nure.apukhtin.hospital.model.Patient;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;

/**
 * Created by Vlad on 19.01.2017.
 */
public class SortingUtils {

    private static final String DOCTOR_SORT_PARAM = "dsort";
    private static final String PATIENT_SORT_PARAM = "psort";
    private static final String PATIENT_ORDER_PARAM = "p_order";
    private static final String DOCTOR_ORDER_PARAM = "d_order";

    public static Comparator<Patient> patientComparatorFromRequest(HttpServletRequest request) {
        String sortParam = request.getParameter(PATIENT_SORT_PARAM);
        String order = request.getParameter(PATIENT_ORDER_PARAM);
        Comparator<Patient> resultComparator = null;

        // no npe in switch
        if (sortParam == null) {
            return Patient.DEFAULT_COMPARATOR;

        }

        switch (sortParam) {
            case "name":
                resultComparator = Patient.NAME_COMPARATOR;
                break;
            case "date":
                resultComparator = Patient.DATE_COMPARATOR;
                break;
            default:
                resultComparator = Patient.DEFAULT_COMPARATOR;
                break;
        }

        if ("desc".equals(order)) {
            resultComparator = resultComparator.reversed();
        }

        return resultComparator;
    }

    public static Comparator<Doctor> doctorComparatorFromRequest(HttpServletRequest request) {
        String docSortParam = request.getParameter(DOCTOR_SORT_PARAM);
        String order = request.getParameter(DOCTOR_ORDER_PARAM);

        Comparator<Doctor> doctorComparator = null;

        if (docSortParam == null) {
            doctorComparator = Doctor.DEFAULT_COMPARATOR;
            return doctorComparator;
        }
        switch (docSortParam) {
            case "name":
                doctorComparator = Doctor.NAME_COMPARATOR;
                break;
            case "category":
                doctorComparator = Doctor.CATEGORY_COMPARATOR;
                break;
            case "patients":
                doctorComparator = Doctor.PATIENT_COUNT_COMPARATOR;
                break;
            default:
                doctorComparator = Doctor.DEFAULT_COMPARATOR;
                break;

        }

        if ("desc".equals(order)) {
            doctorComparator = doctorComparator.reversed();
        }

        return doctorComparator;
    }

}

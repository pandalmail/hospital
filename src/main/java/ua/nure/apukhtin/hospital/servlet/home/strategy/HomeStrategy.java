package ua.nure.apukhtin.hospital.servlet.home.strategy;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vlad on 10.01.2017.
 */
public interface HomeStrategy {

    void redirectHome(HttpServletResponse response) throws IOException;

}

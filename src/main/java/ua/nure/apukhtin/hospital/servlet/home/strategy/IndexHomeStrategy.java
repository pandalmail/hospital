package ua.nure.apukhtin.hospital.servlet.home.strategy;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vlad on 18.01.2017.
 */
public class IndexHomeStrategy implements HomeStrategy {

    private static final String TARGET = "/index.jsp";

    @Override
    public void redirectHome(HttpServletResponse response) throws IOException {
        response.sendRedirect(TARGET);
    }
}

package ua.nure.apukhtin.hospital.servlet;

import ua.nure.apukhtin.hospital.constants.ServerInnerConstants;
import ua.nure.apukhtin.hospital.service.PrescriptionService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.PRESCRIPTION_SERVICE_ATTR;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.error;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.success;

/**
 * Created by Vlad on 22.01.2017.
 */

@WebServlet(name = "DonePrescriptionServlet", urlPatterns = "/execute")
public class DonePrescriptionServlet extends HttpServlet {

    private PrescriptionService prescriptionService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        super.init(config);
        prescriptionService = (PrescriptionService) config.getServletContext().getAttribute(PRESCRIPTION_SERVICE_ATTR);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        String previousUrl = req.getHeader("Referer");
        String prescriptionType = prescriptionTypeFromRequest(req);
        Optional<Long> prescriptionId = getPrescriptionIdFromRequest(req);

        if (!prescriptionId.isPresent()) {
            error(req, resp, previousUrl, Collections.singletonMap("prescription", "error.notfound.descr"));
            return;
        }
        try {
            switch (prescriptionType) {
                case "drugs":
                    prescriptionService.markDrugsAsDone(prescriptionId.get());
                    break;
                case "procedures":
                    prescriptionService.markProcedureAsDone(prescriptionId.get());
                    break;
                case "operations":
                    if (req.isUserInRole(ServerInnerConstants.NURSE_ROLE)) {
                        resp.sendError(HttpServletResponse.SC_FORBIDDEN);
                    }
                    prescriptionService.markOperationAsDone(prescriptionId.get());
                    break;
                default:
                    errors.put("prescription", "error.notpresent.descr");
            }
        } catch (ServiceException e) {
            errors.put("prescription", "error.inner.descr");
        }

        if (errors.isEmpty()) {
            success(req, resp, previousUrl, "prescription.add.success");
        } else {
            error(req, resp, previousUrl, errors);
        }

    }

    private String prescriptionTypeFromRequest(HttpServletRequest req) {
        return Optional.ofNullable(req.getParameter("prescription"))
                .orElse("");
    }

    private Optional<Long> getPrescriptionIdFromRequest(HttpServletRequest request) {
        return Optional.ofNullable(request.getParameter("prescriptionId"))
                .filter(param -> !param.isEmpty())
                .map(Long::valueOf);
    }
}

package ua.nure.apukhtin.hospital.servlet.home.strategy;

import org.apache.log4j.Logger;
import ua.nure.apukhtin.hospital.model.User;

import javax.servlet.http.HttpServletRequest;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;

/**
 * Created by Vlad on 10.01.2017.
 */
public class HomeStrategyFactory {

    private static Logger logger = Logger.getLogger(HomeStrategyFactory.class);

    public static HomeStrategy getByRequest(HttpServletRequest request) {
        if (request.isUserInRole(ADMIN_ROLE)) {
            logger.debug(String.format("User is in role [%s]", ADMIN_ROLE));
            return new AdminHomeStrategy();
        } else if (request.isUserInRole(DOCTOR_ROLE)) {
            return new DocHomeStrategy();
        } else if (request.isUserInRole(NURSE_ROLE)) {
            return new NurseHomeStrategy();
        } else if (request.isUserInRole(PATIENT_ROLE)) {
            User user = (User) request.getSession().getAttribute(USER_ATTR);
            return new PatientHomeStrategy(user.getId());
        } else {
            return new IndexHomeStrategy();
        }
    }

}

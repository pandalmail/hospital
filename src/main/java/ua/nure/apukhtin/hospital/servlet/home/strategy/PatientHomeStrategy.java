package ua.nure.apukhtin.hospital.servlet.home.strategy;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vlad on 25.01.2017.
 */
public class PatientHomeStrategy implements HomeStrategy {

    private static final String PATIENT_URL_FORMAT = "/patient/card/%d";
    private long id;

    PatientHomeStrategy(long id) {
        this.id = id;
    }

    @Override
    public void redirectHome(HttpServletResponse response) throws IOException {
        response.sendRedirect(String.format(PATIENT_URL_FORMAT, id));
    }
}

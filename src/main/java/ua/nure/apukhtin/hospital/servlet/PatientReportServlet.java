package ua.nure.apukhtin.hospital.servlet;

import ua.nure.apukhtin.hospital.constants.ServerInnerConstants;
import ua.nure.apukhtin.hospital.model.User;
import ua.nure.apukhtin.hospital.report.ReportManager;
import ua.nure.apukhtin.hospital.service.IllnessPeriodService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static java.util.Optional.ofNullable;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.*;

/**
 * Created by Vlad on 24.01.2017.
 */

public class PatientReportServlet extends HttpServlet {

    private ReportManager reportManager;
    private IllnessPeriodService illnessPeriodService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        reportManager = (ReportManager) config.getServletContext().getAttribute(REPORT_MANAGER_ATTR);
        illnessPeriodService = (IllnessPeriodService) config.getServletContext().getAttribute(ServerInnerConstants.ILLNESS_PERIOD_ATTR);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        Optional<Long> patientId = getPatientIdFromParam(req);
        Optional<Long> illnessPeriodId = getIdFromParam(ILLNESS_PERIOD_ID_PARAM, req);

        if (!patientId.isPresent()) {
            errors.put("patient", "error.notfound.descr");
        } else {
            User user = (User) req.getSession().getAttribute(USER_ATTR);
            // patient is able to see only its card
            if (req.isUserInRole(PATIENT_ROLE) && !Objects.equals(user.getId(), patientId.get())) {
                resp.sendError(HttpServletResponse.SC_FORBIDDEN);
                return;
            }
        }

        try {
            if (!illnessPeriodId.isPresent()) {
                errors.put("illnessPeriod", "error.notfound.descr");
            } else {
                resp.setContentType("application/pdf");
                ServletOutputStream servletOutputStream = resp.getOutputStream();
                reportManager.writeTo(illnessPeriodId.get(), servletOutputStream);
            }
        } catch (FileNotFoundException | NoSuchFileException e) {
            errors.put("illnessPeriod.report", "error.notfound.descr");
        } catch (IOException | RuntimeException e) {
            errors.put("illnessPeriod.report", "error.inner.descr");
        }

        if (!errors.isEmpty()) {
            String destinationUrl =
                    ofNullable(req.getHeader("Referer"))
                    .orElse(HOME_URL);
            error(req, resp, destinationUrl, errors);
        }
    }

}

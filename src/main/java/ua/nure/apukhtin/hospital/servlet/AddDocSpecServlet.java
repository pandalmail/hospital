package ua.nure.apukhtin.hospital.servlet;

import ua.nure.apukhtin.hospital.service.DoctorCategoryService;
import ua.nure.apukhtin.hospital.service.exception.AlreadyExistsException;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.transaction.TransactionException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.CATEGORY_PARAM;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.DOCTOR_CATEGORY_SERVICE_ATTR;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.error;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.success;

/**
 * Created by Vlad on 15.01.2017.
 */
@WebServlet(name = "AddDocSpecServlet", urlPatterns = "/admin/add/category")
public class AddDocSpecServlet extends HttpServlet {

    private static final String TO = "/home";
    private DoctorCategoryService doctorCategoryService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        doctorCategoryService = (DoctorCategoryService)
                config.getServletContext().getAttribute(DOCTOR_CATEGORY_SERVICE_ATTR);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        String category = request.getParameter(CATEGORY_PARAM);

        try {
            doctorCategoryService.create(category);
        } catch (ServiceException e) {
            TransactionException exception = (TransactionException) e.getCause();
            if (exception.getCause() instanceof AlreadyExistsException) {
                errors.put("admin.register.category.error.exists", "admin.register.category.error.exists.descr");
            } else {
                errors.put("error.inner", "error.inner.descr");
            }
        }

        if(errors.isEmpty()) {
            success(request, response, TO, "admin.register.category.success");
        } else {
            error(request, response, TO, errors);
        }

    }
}

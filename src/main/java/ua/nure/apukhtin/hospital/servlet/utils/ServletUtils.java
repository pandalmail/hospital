package ua.nure.apukhtin.hospital.servlet.utils;

import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;
import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;

/**
 * Created by Vlad on 10.01.2017.
 */
public class ServletUtils {

    private static final int DEFAULT_PAGE = 1;

    public static void forward(HttpServletRequest request, ServletResponse response, String to)
            throws ServletException, IOException {
        request.getRequestDispatcher(to).forward(request, response);
    }

    public static void forward(HttpServletRequest request, ServletResponse response,
                               String to, Map<String, String> errors)
            throws ServletException, IOException {
        request.setAttribute(ERRORS_ATTR, errors);
        request.getRequestDispatcher(to).forward(request, response);
    }

    public static void success(HttpServletRequest request, HttpServletResponse response, String toURL, String message) throws IOException {
        request.getSession().setAttribute(SUCCESS_MSG_ATTR, message);
        response.sendRedirect(toURL);
    }

    public static void error(HttpServletRequest request, HttpServletResponse response,
                             String toURL, Map<String, String> errors) throws IOException {
        request.getSession().setAttribute(ERRORS_ATTR, errors);
        response.sendRedirect(toURL);
    }

    public static Optional<Long> getPatientIdFromParam(HttpServletRequest req) {
        return getIdFromParam(PATIENT_ID_PARAM, req);
    }

    public static Optional<Long> getDocId(HttpServletRequest req) {
        return getIdFromParam(DOCTOR_ID_PARAM, req);
    }

    public static Optional<Long> getIdFromParam(String paramName, HttpServletRequest req) {
        return ofNullable(req.getParameter(paramName))
                .filter(param -> param.matches("\\d+"))
                .map(Long::parseLong);

    }

    // todo replace all page obtainings with this method
    public static int getPageForParam(String paramName, HttpServletRequest req) {
        return ofNullable(req.getParameter(paramName))
                .filter(param -> !param.isEmpty())
                .map(Integer::parseInt)
                .orElse(DEFAULT_PAGE);
    }
}

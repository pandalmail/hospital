package ua.nure.apukhtin.hospital.servlet.home.strategy;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vlad on 10.01.2017.
 */
class AdminHomeStrategy implements HomeStrategy {

    private static final Logger logger = Logger.getLogger(AdminHomeStrategy.class);
    private static final String ADMIN_HOME = "admin";

    @Override
    public void redirectHome(HttpServletResponse response) throws IOException {
        logger.info("Redirection to " + ADMIN_HOME);
        response.sendRedirect(ADMIN_HOME);
    }
}

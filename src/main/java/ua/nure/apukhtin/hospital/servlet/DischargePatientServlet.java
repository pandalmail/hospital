package ua.nure.apukhtin.hospital.servlet;

import ua.nure.apukhtin.hospital.model.IllnessPeriod;
import ua.nure.apukhtin.hospital.model.Patient;
import ua.nure.apukhtin.hospital.report.ReportManager;
import ua.nure.apukhtin.hospital.service.IllnessPeriodService;
import ua.nure.apukhtin.hospital.service.PatientService;
import ua.nure.apukhtin.hospital.service.exception.AlreadyDischargedException;
import ua.nure.apukhtin.hospital.service.exception.NotExistsException;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.servlet.utils.ServletUtils;
import ua.nure.apukhtin.hospital.transaction.TransactionException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static ua.nure.apukhtin.hospital.constants.ServerInnerConstants.*;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.error;
import static ua.nure.apukhtin.hospital.servlet.utils.ServletUtils.success;

/**
 * Created by Vlad on 23.01.2017.
 */

public class DischargePatientServlet extends HttpServlet {

    private PatientService patientService;
    private ReportManager reportManager;
    private IllnessPeriodService illnessPeriodService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        patientService = (PatientService) config.getServletContext().getAttribute(PATIENT_SERVICE_ATTR);
        reportManager = (ReportManager) config.getServletContext().getAttribute(REPORT_MANAGER_ATTR);
        illnessPeriodService = (IllnessPeriodService) config.getServletContext().getAttribute(ILLNESS_PERIOD_ATTR);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        String previousUrl = req.getHeader("Referer");
        Optional<Long> patientId;
        Optional<Long> diagnosisId;

        patientId = ServletUtils.getPatientIdFromParam(req);
        diagnosisId = ServletUtils.getIdFromParam("diagnosisId", req);

        if (!patientId.isPresent()) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        if (!diagnosisId.isPresent()) {
            errors.put("diagnosis", "error.notfound.descr");
        } else {
            try {
                patientService.discharge(patientId.get(), diagnosisId.get());
            } catch (ServiceException e) {
                TransactionException transactionException = ((TransactionException) e.getCause());
                errorForTransactionException(errors, transactionException);
            }
        }

        try {
            IllnessPeriod illnessPeriod = illnessPeriodService.obtainLastForPatient(patientId.get());

            Patient patient = patientService.getById(patientId.get());
            reportManager.generateReport(illnessPeriod, patient);

        } catch (Exception e) {
            errors.put("illnessPeriod.report", "patient.inquiry.create.error.descr");
        }

        if (errors.isEmpty()) {
            success(req, resp, previousUrl, "patient.discharge.successful");
        } else {
            error(req, resp, previousUrl, errors);
        }

    }

    private void errorForTransactionException(Map<String, String> errors, TransactionException transactionException) {
        Throwable cause = transactionException.getCause();
        if (cause instanceof NotExistsException) {
            errors.put("patient", "error.notfound.descr");
        } else if (cause instanceof AlreadyDischargedException) {
            errors.put("patient", "patient.error.discharged.descr");
        }
    }

}

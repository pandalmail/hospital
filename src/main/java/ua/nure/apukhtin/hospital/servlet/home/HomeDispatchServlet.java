package ua.nure.apukhtin.hospital.servlet.home;

import org.apache.log4j.Logger;
import ua.nure.apukhtin.hospital.servlet.home.strategy.HomeStrategy;
import ua.nure.apukhtin.hospital.servlet.home.strategy.HomeStrategyFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vlad on 10.01.2017.
 */
@WebServlet(name = "HomeDispatchServlet", urlPatterns = "/home")
public class HomeDispatchServlet extends HttpServlet {

    private static Logger logger = Logger.getLogger(HomeDispatchServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Entering home servlet with principal: " + request.getUserPrincipal());
        HomeStrategy homeStrategy = HomeStrategyFactory.getByRequest(request);
        if (homeStrategy != null) {
            logger.info("Home url retrieved. Redirecting.");
            homeStrategy.redirectHome(response);
        } else {
            // todo some error
        }
    }
}

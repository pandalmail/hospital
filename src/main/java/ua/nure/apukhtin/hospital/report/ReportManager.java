package ua.nure.apukhtin.hospital.report;

import net.sf.jasperreports.engine.*;
import ua.nure.apukhtin.hospital.config.ApplicationConfig;
import ua.nure.apukhtin.hospital.model.IllnessPeriod;
import ua.nure.apukhtin.hospital.model.Patient;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Vlad on 24.01.2017.
 */
public class ReportManager {

    private static final String PATIENT_NAME = "PATIENT_NAME";
    private static final String FROM_DATE = "FROM_DATE";
    private static final String TO_DATE = "TO_DATE";
    private static final String DIAGNOSIS = "FINAL_DIAGNOSIS";

    private static final String FILE_LOCATION_FORMAT = "%s/%d.pdf";

    private SimpleDateFormat dateFormat;
    private ApplicationConfig config;
    private String reportDestination;
    private String reportLocation;


    public ReportManager(ApplicationConfig config, String reportDestination, String templateReportLocation) {
        this.dateFormat = Objects.requireNonNull((config.getDateFormat()));
        this.config = config;
        this.reportDestination = reportDestination;
        this.reportLocation = templateReportLocation;
    }

    public void generateReport(IllnessPeriod period, Patient patient) throws JRException {
        Map<String, Object> properties = new HashMap<>();
        properties.put(PATIENT_NAME, patient.getName());
        properties.put(FROM_DATE, dateFormat.format(period.getStartDate()));
        properties.put(TO_DATE, dateFormat.format(period.getEndDate()));
        properties.put(DIAGNOSIS, period.getFinalDiagnosis());

        JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(reportLocation, properties, new JREmptyDataSource());
        JasperExportManager.exportReportToPdfFile(jprint, String.format(FILE_LOCATION_FORMAT,
                reportDestination, period.getId()));

        System.out.println("save to: " + String.format(FILE_LOCATION_FORMAT,
                reportDestination, period.getId()));
    }

    public void writeTo(long illnessPeriodId, OutputStream out) throws IOException {
        Path reportPath = Paths.get(String.format(FILE_LOCATION_FORMAT, reportDestination, illnessPeriodId));
        Files.copy(reportPath, out);
    }

}

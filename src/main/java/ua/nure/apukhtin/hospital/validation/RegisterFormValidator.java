package ua.nure.apukhtin.hospital.validation;

import org.apache.log4j.Logger;

import javax.servlet.ServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Vlad on 10.01.2017.
 */
class RegisterFormValidator implements FormValidationCommand {

    private static final String LOGIN_PARAM = "login";
    private static final String NAME_PARAM = "name";
    private static final String PASSWORD_PARAM = "password";

    private static final String NAME_PATTERN = "((\\p{L}+|\\p{L}+-\\p{L}+)\\s?)+";
    private static final String LOGIN_PATTERN = "[\\p{L}\\d]{1,20}";
    private static final String PASSWORD_PATTERN = "(?=.*?\\p{L})(?=.*?\\d).{6,20}";
    private static Logger logger = Logger.getLogger(RegisterFormValidator.class);

    private static final int NAME_LENGTH = 50;

    private String name;
    private String password;
    private String login;

    // todo messages to Resource Bundle
    RegisterFormValidator(ServletRequest request) {
        String login = request.getParameter(LOGIN_PARAM);
        String password = request.getParameter(PASSWORD_PARAM);
        String name = request.getParameter(NAME_PARAM);
        logger.debug(String.format("Extracted login:[%s], password:[%s] and name:[%s] from request",
                login, password, name));

        this.login = login;
        this.password = password;
        this.name = name;
    }

    //todo into resource bundle
    @Override
    public void validate() throws ValidationException {
        Map<String, String> errors = new LinkedHashMap<>();
        logger.info("Beginning validation");

        if (login == null || login.trim().isEmpty()) {
            logger.debug(String.format("User login [%s] not provided", login));
            errors.put("admin.register.doctor.error.login", "error.empty.descr");
        } else if (!login.matches(LOGIN_PATTERN)) {
            logger.debug(String.format("User login [%s] did not pass the validation", login));
            errors.put("admin.register.doctor.error.login", "admin.register.doctor.error.login.descr");
        }

        if (password == null || password.trim().isEmpty()) {
            logger.debug(String.format("Password [%s] not provided", name));
            errors.put("admin.register.doctor.error.password", "error.empty.descr");
        } else if (!(password).matches(PASSWORD_PATTERN)) {
            logger.debug(String.format("Password [%s] did not pass the validation", name));
            errors.put("admin.register.doctor.error.password", "admin.register.doctor.error.password.descr");
        }

        if (name == null || name .trim().isEmpty()) {
            logger.debug(String.format("Name [%s] not provided", name));
            errors.put("admin.register.doctor.error.name", "error.empty.descr");
        } else if (!(name = name.trim()).matches(NAME_PATTERN) || name.length() > NAME_LENGTH) {
            logger.debug(String.format("Name [%s] did not pass the validation", name));
            errors.put("admin.register.doctor.error.name", "admin.register.doctor.error.name.descr");
        }

        if (errors.size() != 0) {
            logger.info("Validation violations have been found: " + errors.keySet());
            throw new ValidationException(errors);
        }
    }
}

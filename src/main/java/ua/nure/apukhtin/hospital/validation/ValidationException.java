package ua.nure.apukhtin.hospital.validation;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vlad on 10.01.2017.
 */
public class ValidationException extends Exception {

    private Map<String, String> errors;

    public ValidationException(String message, Map<String, String> errors) {
        super(message);
        this.errors = errors == null ? new HashMap<>() : errors;
    }

    public ValidationException(Map<String, String> errors) {
        this.errors = errors;
    }

    public Map<String, String> getErrors() {
        return errors;
    }
}

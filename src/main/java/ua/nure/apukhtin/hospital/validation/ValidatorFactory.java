package ua.nure.apukhtin.hospital.validation;

import org.apache.log4j.Logger;

import javax.servlet.ServletRequest;

/**
 * Created by Vlad on 10.01.2017.
 */
public class ValidatorFactory {

    private static final String VALIDATION_PARAM = "validation";
    private static Logger logger = Logger.getLogger(ValidatorFactory.class);

    public static FormValidationCommand fromRequest(ServletRequest request) {
        String validation = request.getParameter(VALIDATION_PARAM);
        if (logger.isDebugEnabled()) {
            logger.debug("Validation param: " + validation);
        }

        if (validation == null) {
            logger.warn("No validation param found in form");
            return null;
        }

        switch (validation) {
            case "register.doctor":
                logger.info("Found login validation. Returning corresponding validator");
                return new RegisterFormValidator(request);
            case "register.category":
                return new CategoryFormValidator(request);
            default:
                logger.warn("Unknown form is specified. No validator available");
                return null;
        }

    }

}

package ua.nure.apukhtin.hospital.validation;

public interface FormValidationCommand {
    void validate() throws ValidationException;
}

package ua.nure.apukhtin.hospital.validation;

import javax.servlet.ServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vlad on 15.01.2017.
 */
public class CategoryFormValidator implements FormValidationCommand {

    private static final String CATEGORY_PATTERN = "[\\p{L}\\(\\)\\s]{1,50}";

    private static final String CATEGORY_PARAM = "category";
    private String name;

    CategoryFormValidator(ServletRequest servletRequest) {
        name = servletRequest.getParameter(CATEGORY_PARAM);
    }


    @Override
    public void validate() throws ValidationException {
        Map<String, String> errors = new HashMap<>();
        if (name == null || (name = name.trim()).isEmpty()) {
            errors.put("admin.register.category.name-field", "error.empty.descr");
        } else if (!name.matches(CATEGORY_PATTERN)) {
            errors.put("admin.register.category.name", "admin.register.category.name.descr");
        }

        if (!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
    }
}

package ua.nure.apukhtin.hospital.service;

import ua.nure.apukhtin.hospital.model.Diagnosis;
import ua.nure.apukhtin.hospital.model.IllnessPeriod;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import java.util.List;

/**
 * Created by Vlad on 20.01.2017.
 */
public interface IllnessPeriodService {

    IllnessPeriod obtainLastOpenedForPatient(long patientId) throws ServiceException;

    List<IllnessPeriod> getForPage(long page, long patientId) throws ServiceException;

    long getMaxPageCount(long patientId) throws ServiceException;

    //todo move to diagnosis service
    List<Diagnosis> getDiagnosisFor(long illnessPeriodId) throws ServiceException;

    IllnessPeriod obtainLastForPatient(Long patientId) throws ServiceException;
}

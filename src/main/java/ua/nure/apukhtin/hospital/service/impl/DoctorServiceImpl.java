package ua.nure.apukhtin.hospital.service.impl;

import org.apache.catalina.realm.RealmBase;
import org.apache.log4j.Logger;
import ua.nure.apukhtin.hospital.config.ApplicationConfig;
import ua.nure.apukhtin.hospital.dao.*;
import ua.nure.apukhtin.hospital.model.*;
import ua.nure.apukhtin.hospital.service.DoctorService;
import ua.nure.apukhtin.hospital.service.IllnessPeriodService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.transaction.TransactionException;
import ua.nure.apukhtin.hospital.transaction.TransactionManager;

import java.text.Collator;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;

/**
 * Created by Vlad on 12.01.2017.
 */
public class DoctorServiceImpl implements DoctorService {

    private static final Comparator<String> CATEGORY_COMPARATOR = Collator.getInstance()::compare;
    private static final int PAGE_OFFSET = 1;
    private static Logger logger = Logger.getLogger(DoctorServiceImpl.class);

    private TransactionManager transactionManager;
    private DoctorDAO doctorDAO;
    private DoctorCategoryDAO doctorCategoryDAO;
    private PrescriptionsDAO prescriptionsDAO;
    private DiagnosisDAO diagnosisDAO;
    private ApplicationConfig applicationConfig;

    private IllnessPeriodService illnessPeriodService;
    private PatientDAO patientDAO;

    public DoctorServiceImpl(TransactionManager transactionManager,
                             DoctorDAO doctorDAO,
                             DoctorCategoryDAO doctorCategoryDAO,
                             ApplicationConfig applicationConfig,
                             PrescriptionsDAO prescriptionsDAO, DiagnosisDAO diagnosisDAO, IllnessPeriodService illnessPeriodService, PatientDAO patientDAO) {
        this.transactionManager = Objects.requireNonNull(transactionManager);
        this.doctorDAO = Objects.requireNonNull(doctorDAO);
        this.doctorCategoryDAO = Objects.requireNonNull(doctorCategoryDAO);
        this.applicationConfig = Objects.requireNonNull(applicationConfig);
        this.prescriptionsDAO = Objects.requireNonNull(prescriptionsDAO);
        this.diagnosisDAO = Objects.requireNonNull(diagnosisDAO);
        this.illnessPeriodService = Objects.requireNonNull(illnessPeriodService);
        this.patientDAO = Objects.requireNonNull(patientDAO);
    }

    @Override
    public Map<String, List<Doctor>> getDoctorsByCategories(Comparator<Doctor> comparator) throws ServiceException {
        logger.info("Try to get all doctors from DB");
        try {
            List<Doctor> allDoctors = transactionManager.inTransaction(() -> doctorDAO.getAll()
                    , doctorDAO);
            List<String> allCategories = transactionManager.inTransaction(() -> doctorCategoryDAO.getAll(),
                    doctorCategoryDAO);
            Collections.sort(allDoctors, comparator);

            Map<String, List<Doctor>> rawCategories = allCategories.stream()
                    .collect(Collectors.toMap(identity(), value -> new ArrayList<Doctor>()));

            Map<String, List<Doctor>> groupedDoctors = allDoctors
                    .stream()
                    .collect(
                            Collectors.groupingBy(Doctor::getCategory, LinkedHashMap::new, Collectors.toList())
                    );

            Map<String, List<Doctor>> result = new LinkedHashMap<>(groupedDoctors);
            rawCategories.forEach((category, docs) -> {
                result.computeIfAbsent(category,
                        emptyCategory -> result.put(emptyCategory, Collections.emptyList()));
            });

            logger.info("Returning groupped allDoctors");
            return result;
        } catch (TransactionException e) {
            logger.warn("Exception within transaction has been found", e);
            throw new ServiceException(e);
        }
    }
    @Override
    public List<Patient> getAssignedPatients(long docId, int page, Comparator<Patient> comparator) throws ServiceException {
        try {
            return transactionManager.inTransaction(() -> {
                int pageSize = applicationConfig.getPageSize();
                int startRow = (page - PAGE_OFFSET) * page;
                Doctor currentDoc = doctorDAO.read(docId);

                List<Patient> docPatients = doctorDAO.getWithDoctor(docId, startRow, pageSize);
                docPatients.sort(comparator);
                docPatients.forEach(doc -> doc.setDoctor(currentDoc));

                return docPatients;
            }, doctorDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Doctor addDoctor(Doctor doctor) throws ServiceException {
        logger.info("Adding doctor in service: " + doctor);

        String previousPassword = doctor.getPassword();
        String encoding = applicationConfig.getPasswordEncoding();
        String digest = applicationConfig.getPasswordDigest();
        String hash = RealmBase.Digest(previousPassword, digest, encoding);

        try {
            return transactionManager.inTransaction(() -> {
                return doctorDAO.create(doctor);
            }, doctorDAO);
        } catch (TransactionException e) {
            logger.warn("Transaction exception has occurred", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void addDiagnosisForPatient(String diagnosisName, long patientId, long docId, Prescriptions prescriptions)
            throws ServiceException {
        try {
            IllnessPeriod illnessPeriod = illnessPeriodService.obtainLastOpenedForPatient(patientId);
            transactionManager.<Void>inTransaction(() -> {
                Diagnosis diagnosis = prepareDiagnosis(diagnosisName, patientId);
                diagnosis = diagnosisDAO.createForIllnessPeriod(diagnosis, illnessPeriod, patientId, docId);
                prescriptionsDAO.createForDiagnosis(prescriptions, diagnosis.getId());
                patientDAO.setDischarged(patientId, false);
                return null;
            }, diagnosisDAO, prescriptionsDAO, patientDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    private Diagnosis prepareDiagnosis(String diagnosisName, long userId) {
        Diagnosis diagnosis = new Diagnosis();
        diagnosis.setName(diagnosisName);

        return diagnosis;
    }
}
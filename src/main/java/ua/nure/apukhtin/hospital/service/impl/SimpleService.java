package ua.nure.apukhtin.hospital.service.impl;

import ua.nure.apukhtin.hospital.transaction.TransactionManager;

import java.util.Objects;

/**
 * Created by Vlad on 20.01.2017.
 */
public abstract class SimpleService {

    protected TransactionManager transactionManager;

    public SimpleService(TransactionManager transactionManager) {
        this.transactionManager = Objects.requireNonNull(transactionManager);
    }
}

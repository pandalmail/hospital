package ua.nure.apukhtin.hospital.service.impl;

import ua.nure.apukhtin.hospital.dao.PrescriptionsDAO;
import ua.nure.apukhtin.hospital.model.Prescriptions;
import ua.nure.apukhtin.hospital.service.PrescriptionService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.transaction.TransactionException;
import ua.nure.apukhtin.hospital.transaction.TransactionManager;

import java.util.Objects;

/**
 * Created by Vlad on 22.01.2017.
 */
public class PrescriptionServiceImpl extends SimpleService implements PrescriptionService {

    private PrescriptionsDAO prescriptionsDAO;

    public PrescriptionServiceImpl(TransactionManager transactionManager, PrescriptionsDAO prescriptionsDAO) {
        super(transactionManager);
        this.prescriptionsDAO = Objects.requireNonNull(prescriptionsDAO);
    }

    @Override
    public Prescriptions getForDiagnosis(long diagnosisId) throws ServiceException {
        try {
            return transactionManager.inTransaction(() -> prescriptionsDAO.getPrescriptionsFor(diagnosisId),
                    prescriptionsDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void markProcedureAsDone(long prescriptionId) throws ServiceException {
        try {
            transactionManager.inTransaction(() -> {
                prescriptionsDAO.makeProceduresDone(prescriptionId);
                return null;
            }, prescriptionsDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void markDrugsAsDone(long prescriptionId) throws ServiceException {
        try {
            transactionManager.inTransaction(() -> {
                prescriptionsDAO.makeDrugsDone(prescriptionId);
                return null;
            }, prescriptionsDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }

    }

    @Override
    public void markOperationAsDone(long prescriptionId) throws ServiceException {
        try {
            transactionManager.inTransaction(() -> {
                prescriptionsDAO.makeOperationsDone(prescriptionId);
                return null;
            }, prescriptionsDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }

    }
}

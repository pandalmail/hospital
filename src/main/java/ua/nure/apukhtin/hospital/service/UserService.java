package ua.nure.apukhtin.hospital.service;

import ua.nure.apukhtin.hospital.model.User;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

/**
 * Created by Vlad on 19.01.2017.
 */
public interface UserService {

    User getByLogin(String login) throws ServiceException;

}

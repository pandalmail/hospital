package ua.nure.apukhtin.hospital.service;

import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import java.util.List;

/**
 * Created by Vlad on 15.01.2017.
 */
public interface DoctorCategoryService {

    void create(String name) throws ServiceException;

    List<String> getAll() throws ServiceException;

}

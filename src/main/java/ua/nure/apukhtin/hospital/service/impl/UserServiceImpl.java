package ua.nure.apukhtin.hospital.service.impl;

import ua.nure.apukhtin.hospital.dao.UserDAO;
import ua.nure.apukhtin.hospital.model.User;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.service.UserService;
import ua.nure.apukhtin.hospital.transaction.TransactionException;
import ua.nure.apukhtin.hospital.transaction.TransactionManager;

import java.util.Objects;

/**
 * Created by Vlad on 19.01.2017.
 */
public class UserServiceImpl implements UserService {

    private TransactionManager transactionManager;
    private UserDAO userDAO;

    public UserServiceImpl(TransactionManager transactionManager, UserDAO userDAO) {
        this.transactionManager = Objects.requireNonNull(transactionManager);
        this.userDAO = userDAO;
    }

    @Override
    public User getByLogin(String login) throws ServiceException {
        try {
            return transactionManager.inTransaction(() -> userDAO.getByLogin(login), userDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }
}

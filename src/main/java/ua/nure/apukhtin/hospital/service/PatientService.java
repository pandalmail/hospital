package ua.nure.apukhtin.hospital.service;

import ua.nure.apukhtin.hospital.model.Patient;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Vlad on 11.01.2017.
 */
public interface PatientService {

    void addPatient(Patient patient) throws ServiceException;

    default List<Patient> getPatients(int page) throws ServiceException {
        return getPatients(page, (o1, o2) -> 0);
    }

    Patient getById(Long id) throws ServiceException;

    List<Patient> getPatients(long page, Comparator<Patient> comparator) throws ServiceException;

    void assignWithDoctor(Long docId, long uId) throws ServiceException;

    long getMaxPageCount() throws ServiceException;

    void discharge(long patientId, long finalDiagnosis) throws ServiceException;

}

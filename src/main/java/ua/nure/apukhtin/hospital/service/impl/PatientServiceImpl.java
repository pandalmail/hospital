package ua.nure.apukhtin.hospital.service.impl;

import org.apache.catalina.realm.RealmBase;
import ua.nure.apukhtin.hospital.config.ApplicationConfig;
import ua.nure.apukhtin.hospital.dao.DoctorDAO;
import ua.nure.apukhtin.hospital.dao.IllnessPeriodDAO;
import ua.nure.apukhtin.hospital.dao.PatientDAO;
import ua.nure.apukhtin.hospital.model.IllnessPeriod;
import ua.nure.apukhtin.hospital.model.Patient;
import ua.nure.apukhtin.hospital.service.PatientService;
import ua.nure.apukhtin.hospital.service.exception.AlreadyDischargedException;
import ua.nure.apukhtin.hospital.service.exception.AlreadyExistsException;
import ua.nure.apukhtin.hospital.service.exception.NotExistsException;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.transaction.TransactionException;
import ua.nure.apukhtin.hospital.transaction.TransactionManager;

import java.util.*;

/**
 * Created by Vlad on 16.01.2017.
 */
public class PatientServiceImpl implements PatientService {

    private static final int PAGE_OFFSET = 1;
    private TransactionManager transactionManager;
    private PatientDAO patientDAO;
    private DoctorDAO doctorDAO;

    private ApplicationConfig applicationConfig;
    private IllnessPeriodDAO illnessPeriodDAO;

    public PatientServiceImpl(TransactionManager transactionManager,
                              PatientDAO patientDAO,
                              ApplicationConfig applicationConfig, DoctorDAO doctorDAO, IllnessPeriodDAO illnessPeriodDAO) {

        this.transactionManager = Objects.requireNonNull(transactionManager);
        this.patientDAO = Objects.requireNonNull(patientDAO);
        this.applicationConfig = Objects.requireNonNull(applicationConfig);
        this.doctorDAO = Objects.requireNonNull(doctorDAO);
        this.illnessPeriodDAO = Objects.requireNonNull(illnessPeriodDAO);
    }

    @Override
    public void addPatient(Patient patient) throws ServiceException {
        String passwordEncoding = applicationConfig.getPasswordEncoding();
        String algorithm = applicationConfig.getPasswordDigest();
        String plainPassword = patient.getPassword();

        patient.setPassword(RealmBase.Digest(plainPassword, algorithm, passwordEncoding));
        try {
            transactionManager.inTransaction(() -> {
                if (!patientDAO.exists(patient.getId())) {
                    return patientDAO.create(patient);
                } else {
                    throw new AlreadyExistsException();
                }
            }, patientDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }

    }

    @Override
    public void assignWithDoctor(Long docId, long uId) throws ServiceException {
        try {
            transactionManager.<Void>inTransaction(() -> {
                if (patientDAO.exists(uId) && doctorDAO.exists(docId)) {
                    patientDAO.assignWithDoctor(docId, uId);
                } else {
                    throw new NotExistsException("patient or doctor do not exist");
                }
                return null;
            }, patientDAO, doctorDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Patient getById(Long id) throws ServiceException {
        try {
            return transactionManager.inTransaction(() -> {
                if (!patientDAO.exists(id)) {
                    throw new NotExistsException("Patient with id does not exist " + id);
                }
                return patientDAO.read(id);
            }, patientDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Patient> getPatients(long page, Comparator<Patient> comparator) throws ServiceException {
        long pageSize = applicationConfig.getPageSize();
        long offset = (page - PAGE_OFFSET) * pageSize;
        try {
            List<Patient> patients = transactionManager.inTransaction(() -> patientDAO.getWithLimit(offset, pageSize), patientDAO);
            Collections.sort(patients, comparator);
            return patients;
        } catch (TransactionException e) {
            throw new ServiceException();
        }
    }

    @Override
    public long getMaxPageCount() throws ServiceException {
        long pageSize = applicationConfig.getPageSize();
        try {
            long patientCount =
                    transactionManager.inTransaction(() -> patientDAO.count(), patientDAO);
            return (long) Math.ceil((double) patientCount / pageSize);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void discharge(long patientId, long finalDiagnosisId) throws ServiceException {
        Date today = new Date();

        try {
            transactionManager.<Void>inTransaction(() -> {
                if(!patientDAO.exists(patientId)) {
                    throw new NotExistsException();
                }
                patientDAO.setDischarged(patientId, true);
                IllnessPeriod lastOpened = illnessPeriodDAO.getLastOpened(patientId);
                if (lastOpened == null) {
                    throw new AlreadyDischargedException();
                }

                illnessPeriodDAO.closeWithDate(lastOpened.getId(), today, finalDiagnosisId);
                return null;
            }, patientDAO, illnessPeriodDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }
}

package ua.nure.apukhtin.hospital.service;

import ua.nure.apukhtin.hospital.model.Doctor;
import ua.nure.apukhtin.hospital.model.Patient;
import ua.nure.apukhtin.hospital.model.Prescriptions;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Created by Vlad on 11.01.2017.
 */
public interface DoctorService {

    default Map<String, List<Doctor>> getDoctorsByCategories() throws ServiceException {
        return getDoctorsByCategories((one, another) -> 0);
    }

    Map<String, List<Doctor>> getDoctorsByCategories(Comparator<Doctor> comparator) throws ServiceException;

    List<Patient> getAssignedPatients(long docId, int page, Comparator<Patient> comparator) throws ServiceException;

    Doctor addDoctor(Doctor doctor) throws ServiceException;

    default List<Patient> getAssignedPatients(long docId) throws ServiceException {
        return getAssignedPatients(docId, 1, (one, another) -> 0);
    }


    void addDiagnosisForPatient(String diagnosisName, long patientId, long docId, Prescriptions prescriptions) throws ServiceException;
}

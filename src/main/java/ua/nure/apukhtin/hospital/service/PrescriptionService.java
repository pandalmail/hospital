package ua.nure.apukhtin.hospital.service;

import ua.nure.apukhtin.hospital.model.Prescriptions;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

/**
 * Created by Vlad on 21.01.2017.
 */
public interface PrescriptionService {

    Prescriptions getForDiagnosis(long diagnosisId) throws ServiceException;

    void markProcedureAsDone(long prescriptionId) throws ServiceException;

    void markDrugsAsDone(long prescriptionId) throws ServiceException;

    void markOperationAsDone(long prescriptionId) throws ServiceException;

}

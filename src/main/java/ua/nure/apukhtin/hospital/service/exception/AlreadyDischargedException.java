package ua.nure.apukhtin.hospital.service.exception;

/**
 * Created by Vlad on 23.01.2017.
 */
public class AlreadyDischargedException extends Exception {

    public AlreadyDischargedException() {
    }

    public AlreadyDischargedException(String message) {
        super(message);
    }

    public AlreadyDischargedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AlreadyDischargedException(Throwable cause) {
        super(cause);
    }

    public AlreadyDischargedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

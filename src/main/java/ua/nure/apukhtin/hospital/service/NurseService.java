package ua.nure.apukhtin.hospital.service;

import ua.nure.apukhtin.hospital.model.Nurse;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;

import java.util.List;

/**
 * Created by Vlad on 24.01.2017.
 */
public interface NurseService {

    Nurse create(Nurse nurse) throws ServiceException;

    List<Nurse> getAll() throws ServiceException;

}

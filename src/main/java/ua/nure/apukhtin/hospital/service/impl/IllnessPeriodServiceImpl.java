package ua.nure.apukhtin.hospital.service.impl;

import ua.nure.apukhtin.hospital.config.ApplicationConfig;
import ua.nure.apukhtin.hospital.dao.DiagnosisDAO;
import ua.nure.apukhtin.hospital.dao.IllnessPeriodDAO;
import ua.nure.apukhtin.hospital.dao.PatientDAO;
import ua.nure.apukhtin.hospital.dao.exceptions.DAOException;
import ua.nure.apukhtin.hospital.model.Diagnosis;
import ua.nure.apukhtin.hospital.model.IllnessPeriod;
import ua.nure.apukhtin.hospital.service.IllnessPeriodService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.transaction.TransactionException;
import ua.nure.apukhtin.hospital.transaction.TransactionManager;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by Vlad on 20.01.2017.
 */
public class IllnessPeriodServiceImpl extends SimpleService implements IllnessPeriodService {

    private static final long PAGE_OFFSET = 1;
    private IllnessPeriodDAO illnessPeriodDAO;
    private ApplicationConfig applicationConfig;
    private DiagnosisDAO diagnosisDAO;
    private PatientDAO patientDAO;

    public IllnessPeriodServiceImpl(TransactionManager transactionManager,
                                    IllnessPeriodDAO illnessPeriodDAO, ApplicationConfig applicationConfig, DiagnosisDAO diagnosisDao, PatientDAO patientDAO) {
        super(transactionManager);
        this.illnessPeriodDAO = Objects.requireNonNull(illnessPeriodDAO);
        this.applicationConfig = Objects.requireNonNull(applicationConfig);
        this.diagnosisDAO  = Objects.requireNonNull(diagnosisDao);
        this.patientDAO = Objects.requireNonNull(patientDAO);
    }

    // todo possible db function
    @Override
    public IllnessPeriod obtainLastOpenedForPatient(long patientId) throws ServiceException {
        try {
            return transactionManager.inTransaction(() -> {
                if (illnessPeriodDAO.hasLastOpened(patientId)) {
                    return illnessPeriodDAO.getLastOpened(patientId);
                } else {
                    IllnessPeriod illnessPeriod = createPeriodWithPatient(patientId);
                    patientDAO.setDischarged(patientId, false);
                    return illnessPeriodDAO.create(illnessPeriod, patientId);
                }
            }, illnessPeriodDAO, patientDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<IllnessPeriod> getForPage(long page, long patientId) throws ServiceException {
        long pageSize = applicationConfig.getPageSize();
        long offset = (page - PAGE_OFFSET) * pageSize;

        try {
            return transactionManager.inTransaction(() -> {
                List<IllnessPeriod> periods = illnessPeriodDAO.getWithLimit(offset, pageSize, patientId);
                injectFinalDiagnoses(periods, diagnosisDAO);
                return periods;
            }, illnessPeriodDAO, diagnosisDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    private void injectFinalDiagnoses(List<IllnessPeriod> periods, DiagnosisDAO diagnosisDAO) throws DAOException {
        for (IllnessPeriod illnessPeriod: periods) {
            Diagnosis diagnosis = diagnosisDAO.getIllnessPeriodFinalDiagnosis(illnessPeriod.getId());
            Optional<String> diagnosisName = Optional.ofNullable(diagnosis)
                    .map(Diagnosis::getName);
            diagnosisName.ifPresent(illnessPeriod::setFinalDiagnosis);
        }
    }

    @Override
    public long getMaxPageCount(long patientId) throws ServiceException {
        long pageSize = applicationConfig.getPageSize();
        long count = 0;

        try {
            count = transactionManager.inTransaction(() -> illnessPeriodDAO.count(patientId), illnessPeriodDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }

        return (long) Math.ceil((double) count / pageSize);
    }

    private IllnessPeriod createPeriodWithPatient(Long patientId) {
        IllnessPeriod illnessPeriod = new IllnessPeriod();
        illnessPeriod.setStartDate(new Date());
        return illnessPeriod;
    }

    @Override
    public List<Diagnosis> getDiagnosisFor(long illnessPeriodId) throws ServiceException {
        try {
            return transactionManager.inTransaction(() -> diagnosisDAO.getIllnessPeriodDiagnosis(illnessPeriodId),
                    diagnosisDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public IllnessPeriod obtainLastForPatient(Long patientId) throws ServiceException {
        try {
            return transactionManager.inTransaction(() -> {
                        IllnessPeriod last = illnessPeriodDAO.getLast(patientId);
                        Diagnosis finalDiagnosis = diagnosisDAO.getIllnessPeriodFinalDiagnosis(last.getId());
                        last.setFinalDiagnosis(finalDiagnosis.getName());

                        return last;
                    },
                    illnessPeriodDAO, diagnosisDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }
}

package ua.nure.apukhtin.hospital.service.impl;

import org.apache.catalina.realm.RealmBase;
import ua.nure.apukhtin.hospital.config.ApplicationConfig;
import ua.nure.apukhtin.hospital.dao.NurseDAO;
import ua.nure.apukhtin.hospital.model.Nurse;
import ua.nure.apukhtin.hospital.service.NurseService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.transaction.TransactionException;
import ua.nure.apukhtin.hospital.transaction.TransactionManager;

import java.util.List;
import java.util.Objects;

/**
 * Created by Vlad on 24.01.2017.
 */
public class NurseServiceImpl extends SimpleService implements NurseService {

    private ApplicationConfig config;
    private NurseDAO nurseDAO;

    public NurseServiceImpl(TransactionManager transactionManager, NurseDAO nurseDAO, ApplicationConfig config) {
        super(transactionManager);
        this.nurseDAO = Objects.requireNonNull(nurseDAO);
        this.config = Objects.requireNonNull(config);
    }

    @Override
    public Nurse create(Nurse nurse) throws ServiceException {
        String hashed = RealmBase.Digest(nurse.getPassword(),
                config.getPasswordDigest(),
                config.getPasswordEncoding());
        nurse.setPassword(hashed);
        try {
            return transactionManager.inTransaction(() -> nurseDAO.create(nurse), nurseDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Nurse> getAll() throws ServiceException {
        try {
            return transactionManager.inTransaction(() -> nurseDAO.getAll(), nurseDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }
}

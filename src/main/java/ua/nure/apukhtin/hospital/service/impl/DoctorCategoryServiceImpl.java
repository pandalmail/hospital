package ua.nure.apukhtin.hospital.service.impl;

import ua.nure.apukhtin.hospital.dao.DoctorCategoryDAO;
import ua.nure.apukhtin.hospital.service.DoctorCategoryService;
import ua.nure.apukhtin.hospital.service.exception.ServiceException;
import ua.nure.apukhtin.hospital.service.exception.AlreadyExistsException;
import ua.nure.apukhtin.hospital.transaction.TransactionException;
import ua.nure.apukhtin.hospital.transaction.TransactionManager;

import java.util.List;
import java.util.Objects;

/**
 * Created by Vlad on 15.01.2017.
 */
public class DoctorCategoryServiceImpl implements DoctorCategoryService {

    private DoctorCategoryDAO doctorCategoryDAO;
    private TransactionManager transactionManager;

    public DoctorCategoryServiceImpl(DoctorCategoryDAO doctorCategoryDAO, TransactionManager transactionManager) {
        this.doctorCategoryDAO = Objects.requireNonNull(doctorCategoryDAO);
        this.transactionManager = Objects.requireNonNull(transactionManager);
    }

    @Override
    public void create(String name) throws ServiceException {
        try {
            transactionManager.inTransaction(() -> {
                if (!doctorCategoryDAO.exists(name)) {
                    return doctorCategoryDAO.create(name);
                } else {
                    throw new AlreadyExistsException();
                }
            }, doctorCategoryDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<String> getAll() throws ServiceException {
        try {
            return transactionManager.inTransaction(() -> doctorCategoryDAO.getAll(), doctorCategoryDAO);
        } catch (TransactionException e) {
            throw new ServiceException(e);
        }
    }
}
